import Vue from 'vue'
import App from './App'
import * as filters from './dev/core/utils/filters.util' // global filters
import httpInterceptor from './dev/core/interceptor/http.interceptor.js'
import uView from 'uview-ui'
import myMixin from "./dev/core/mixin/my.mixin";
import globalVariable from "./dev/core/global-variable";

Vue.config.productionTip = false;

App.mpType = 'app';

Vue.use(uView);

// 注册全局的过滤器，在页面上直接使用无须再显示定义过滤器
Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key]);
    Vue.prototype[key] = filters[key];  // 自定义函数，在所有vue中可以直接 this.函数名 使用
});

// 自定义函数，在所有vue中可以直接 this.函数名 使用 todo <template/>模板页面中无法直接使用，js中可以直接使用（uview框架bug） data()方法中可以使用this.globalVariable引用
Vue.prototype.globalVariable = globalVariable;

// 混入，全局引用
Vue.mixin(myMixin);

const app = new Vue({
    ...App
});

// http拦截器，将此部分放在new Vue()和app.$mount()之间，才能App.vue中正常使用
Vue.use(httpInterceptor, app);

app.$mount();
