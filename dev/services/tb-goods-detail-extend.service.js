import {BaseService} from '../core/base/base.service'

export class TbGoodsDetailExtendService extends BaseService {

    findTableTbGoodsListCascade(params) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailextend/findTableTbGoodsListCascade?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTbGoodsDetailExtendList(tbGoodsDetailExtend) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailextend/findList`,
            data: tbGoodsDetailExtend
        })
    }

    findTransferTbGoodsDetailExtendList(tbGoodsDetailExtend) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailextend/findTransferTbGoodsDetailExtendList`,
            data: tbGoodsDetailExtend
        })
    }

    saveTbGoodsDetailExtend(tbGoodsDetailExtend) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailextend/save`,
            data: tbGoodsDetailExtend
        })
    }

    batchSaveAssignedTbGoodsDetailExtends(tbGoodsDetailExtend) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailextend/batchSaveAssignedTbGoodsDetailExtends`,
            data: tbGoodsDetailExtend
        })
    }

    updateTbGoodsDetailExtend(tbGoodsDetailExtend) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailextend/update`,
            data: tbGoodsDetailExtend
        })
    }

    batchDeleteTbGoodsDetailExtendByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailextend/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }

    batchDelAssignedTbGoodsDetailExtends(goodsIds) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailextend/batchDelAssignedTbGoodsDetailExtends`,
            data: {goodsIds: goodsIds}
        })
    }
}
