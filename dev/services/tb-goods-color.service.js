import {BaseService} from '../core/base/base.service'

export class TbGoodsColorService extends BaseService {

    findTbGoodsColorList(tbGoodsColor) {
        return this.httpUtil.myPost({
            url: `tbgoodscolor/findList`,
            data: tbGoodsColor
        })
    }

    saveTbGoodsColor(tbGoodsColor) {
        return this.httpUtil.myPost({
            url: `tbgoodscolor/save`,
            data: tbGoodsColor
        })
    }

    updateTbGoodsColor(tbGoodsColor) {
        return this.httpUtil.myPost({
            url: `tbgoodscolor/update`,
            data: tbGoodsColor
        })
    }

    batchDeleteTbGoodsColorByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbgoodscolor/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }
}
