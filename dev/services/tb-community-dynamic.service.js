import {BaseService} from "../core/base/base.service";

export class TbCommunityDynamicService extends BaseService {

    findTbCommunityDynamicById(id) {
        return this.httpUtil.myPost({
            url: `tbcommunitydynamic/findTbCommunityDynamicById`,
            data: {id: id},
        });
    }

    findTbCommunityDynamicList(tbCommunityDynamic) {
        return this.httpUtil.myPost({
            url: `tbcommunitydynamic/findList`,
            data: tbCommunityDynamic,
            orderBy: 'display_order asc'
        });
    }

    findTableList(params) {
        return this.httpUtil.myPost({
            url: `tbcommunitydynamic/findTableList?${params.pagination.appendToUrl()}`,
            data: params.data,
        });
    }

    findTableTbCommunityDynamicList(params) {
        return this.httpUtil.myPost({
            url: `tbcommunitydynamic/findTableTbCommunityDynamicList?${params.pagination.appendToUrl()}`,
            data: params.data,
        });
    }

    saveTbCommunityDynamic(tbCommunityDynamic) {
        return this.httpUtil.myPost({
            url: `tbcommunitydynamic/save`,
            data: tbCommunityDynamic
        });
    }

    updateTbCommunityDynamic(tbCommunityDynamic) {
        return this.httpUtil.myPost({
            url: `tbcommunitydynamic/update`,
            data: tbCommunityDynamic
        });
    }

    deleteTbCommunityDynamicByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbcommunitydynamic/deleteByIds`,
            data: {ids: ids}
        });
    }
}
