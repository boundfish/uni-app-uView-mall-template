import {BaseService} from '../core/base/base.service'

export class TbStoreCashDepositRechargeRecordService extends BaseService {

    findTbStoreCashDepositRechargeRecordList(tbStoreCashDepositRechargeRecord) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositrechargerecord/findList`,
            data: tbStoreCashDepositRechargeRecord
        })
    }

    findTableList(params) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositrechargerecord/findTableList?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTableTbStoreCashDepositRechargeRecordList(params) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositrechargerecord/findTableTbStoreCashDepositRechargeRecordList?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    saveTbStoreCashDepositRechargeRecord(tbStoreCashDepositRechargeRecord) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositrechargerecord/save`,
            data: tbStoreCashDepositRechargeRecord
        })
    }

    updateTbStoreCashDepositRechargeRecord(tbStoreCashDepositRechargeRecord) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositrechargerecord/update`,
            data: tbStoreCashDepositRechargeRecord
        })
    }

    findTbStoreCashDepositRechargeRecordById(id) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositrechargerecord/findById`,
            data: {id: id}
        })
    }

    findOne(tbStoreCashDepositRechargeRecord) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositrechargerecord/findOne`,
            data: tbStoreCashDepositRechargeRecord
        })
    }
}
