import {BaseService} from '../core/base/base.service'

export class TbOrderGoodsService extends BaseService {

    findTableListCascade(params) {
        return this.httpUtil.myPost({
            url: `tbordergoods/findTableList?${params.pagination.appendToUrl()}`,
            data: params.data,
        })
    }

    findTbOrderGoodsById(id) {
        return this.httpUtil.myPost({
            url: `tbordergoods/findById`,
            data: {id: id}
        })
    }

    saveTbOrderGoods(tbOrderGoods) {
        return this.httpUtil.myPost({
            url: `tbordergoods/save`,
            data: tbOrderGoods
        })
    }

    updateTbOrderGoods(tbOrderGoods) {
        return this.httpUtil.myPost({
            url: `tbordergoods/update`,
            data: tbOrderGoods
        })
    }

    batchDeleteTbOrderGoodsByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbordergoods/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }
}
