import {BaseService} from '../core/base/base.service'

export class TbGoodsDetailGoodsRecommendTemplateService extends BaseService {

    findTableTbGoodsListCascade(params) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailgoodsrecommendtemplate/findTableTbGoodsListCascade?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTbGoodsDetailGoodsRecommendTemplateList(tbGoodsDetailGoodsRecommendTemplate) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailgoodsrecommendtemplate/findList`,
            data: tbGoodsDetailGoodsRecommendTemplate
        })
    }

    findTransferTbGoodsDetailGoodsRecommendTemplateList(tbGoodsDetailGoodsRecommendTemplate) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailgoodsrecommendtemplate/findTransferTbGoodsDetailGoodsRecommendTemplateList`,
            data: tbGoodsDetailGoodsRecommendTemplate
        })
    }

    findGoodsListByGoodsDetailGoodsRecommendTemplateId(tbGoodsDetailGoodsRecommendTemplate) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailgoodsrecommendtemplate/findGoodsListByGoodsDetailGoodsRecommendTemplateId`,
            data: tbGoodsDetailGoodsRecommendTemplate
        })
    }

    saveTbGoodsDetailGoodsRecommendTemplate(tbGoodsDetailGoodsRecommendTemplate) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailgoodsrecommendtemplate/save`,
            data: tbGoodsDetailGoodsRecommendTemplate
        })
    }

    batchSaveAssignedTbGoodsDetailGoodsRecommendTemplates(tbGoodsDetailGoodsRecommendTemplate) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailgoodsrecommendtemplate/batchSaveAssignedTbGoodsDetailGoodsRecommendTemplates`,
            data: tbGoodsDetailGoodsRecommendTemplate
        })
    }

    updateTbGoodsDetailGoodsRecommendTemplate(tbGoodsDetailGoodsRecommendTemplate) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailgoodsrecommendtemplate/update`,
            data: tbGoodsDetailGoodsRecommendTemplate
        })
    }

    batchDeleteTbGoodsDetailGoodsRecommendTemplateByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailgoodsrecommendtemplate/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }

    batchDelAssignedTbGoodsDetailGoodsRecommendTemplates(goodsIds) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailgoodsrecommendtemplate/batchDelAssignedTbGoodsDetailGoodsRecommendTemplates`,
            data: {goodsIds: goodsIds}
        })
    }
}
