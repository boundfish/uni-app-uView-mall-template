import {BaseService} from "../core/base/base.service";

export class TbCommunityLotteryUserService extends BaseService {

    findTbCommunityLotteryUserList(tbCommunityLotteryUser) {
        return this.httpUtil.myPost({
            url: `tbcommunitylotteryuser/findList`,
            data: tbCommunityLotteryUser,
        });
    }

    findTableListCascade(param) {
        return this.httpUtil.myPost({
            url: `tbcommunitylotteryuser/findTableListCascade?${params.pagination.appendToUrl()}`,
            data: params.data,
        });
    }

    findTbCommunityLotteryUser(tbCommunityLotteryUser) {
        return this.httpUtil.myPost({
            url: `tbcommunitylotteryuser/findOne`,
            data: tbCommunityLotteryUser,
        });
    }

    findTbCommunityLotteryUserById(id) {
        return this.httpUtil.myPost({
            url: `tbcommunitylotteryuser/findById`,
            data: {id: id},
        });
    }

    statisticsMoney(data) {
        return this.httpUtil.myPost({
            url: `tbcommunitylotteryuser/statisticsMoney`,
            data: data,
        });
    }

    saveTbCommunityLotteryUser(tbCommunityLotteryUser) {
        return this.httpUtil.myPost({
            url: `tbcommunitylotteryuser/save`,
            data: tbCommunityLotteryUser
        });
    }

    updateTbCommunityLotteryUser(tbCommunityLotteryUser) {
        return this.httpUtil.myPost({
            url: `tbcommunitylotteryuser/update`,
            data: tbCommunityLotteryUser
        });
    }

    deleteTbCommunityLotteryUserById(id) {
        return this.httpUtil.myPost({
            url: `tbcommunitylotteryuser/deleteById`,
            data: {id: id}
        });
    }
}
