import {BaseService} from '../core/base/base.service'

export class LoginService extends BaseService {

    login(params) {
        return this.httpUtil.myPost({
            url: 'sysuser/login',
            data: {
                username: params.username,
                password: params.password
            }
        })
    }

    /**
     * 扫码登陆
     * @param data
     * @returns {*}
     */
    loginByScan(data) {
        return this.httpUtil.myPost({
            url: 'sysuser/loginByScan',
            data: data
        })
    }

    /**
     * 从服务号登陆
     * @param data
     * @returns {*}
     */
    loginByFwh(data) {
        return this.httpUtil.myPost({
            url: 'sysuser/loginByFwh',
            data: data
        })
    }

    /**
     * 小程序登录
     * @param data
     * @returns {*}
     */
    miniAppLogin(data) {
        return this.httpUtil.myPost({
            url: 'wechat/miniAppLogin',
            data: data
        })
    }

    gotoIndexPage() {
        if (!this.globalVariable.userInfo.tbCommunityUser || this.globalVariable.userInfo.tbCommunityUser.type === '1') {
            uni.showToast({title: '登录成功，即将跳转到首页', icon: 'none'})
            uni.navigateTo({
                url: `/pages/sub/sub1-tabs/index`
            })
        }
    }
}
