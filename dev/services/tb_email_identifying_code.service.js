import {BaseService} from '../core/base/base.service'

export class TbEmailIdentifyingCodeService extends BaseService {

    sendMailIdentifyingCode(email) {
        return this.httpUtil.myPost({
            url: `tbemailidentifyingcode/sendMailIdentifyingCode`,
            data: {email: email}
        })
    }

    sendMailIdentifyingCodeByUsername(username) {
        return this.httpUtil.myPost({
            url: `tbemailidentifyingcode/sendMailIdentifyingCodeByUsername`,
            data: {username: username}
        })
    }

    mailIdentifyingCodeCheck(email, identifyingCode) {
        return this.httpUtil.myPost({
            url: `tbemailidentifyingcode/mailIdentifyingCodeCheck`,
            data: {
                email: email,
                identifyingCode: identifyingCode
            }
        })
    }
}
