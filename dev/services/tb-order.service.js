import {BaseService} from '../core/base/base.service'

export class TbOrderService extends BaseService {
    findTableListCascade(params) {
        return this.httpUtil.myPost({
            url: `tborder/findTableListCascade?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTableListCascadeByGoodsAfterSaleApply(params) {
        return this.httpUtil.myPost({
            url: `tborder/findTableListCascadeByGoodsAfterSaleApply?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTbOrderList(tbOrder) {
        return this.httpUtil.myPost({
            url: `tborder/findListCascade`,
            data: tbOrder
        })
    }

    findTbOrderById(id) {
        return this.httpUtil.myPost({
            url: `tborder/findById`,
            data: {id: id}
        })
    }

    findCount(tbOrder) {
        return this.httpUtil.myPost({
            url: `tborder/findCount`,
            data: tbOrder
        })
    }

    prepareTbOrder(tbOrder) {
        return this.httpUtil.myPostJson({
            url: `tborder/prepareTbOrder`,
            data: tbOrder
        })
    }

    saveTbOrder(tbOrder) {
        return this.httpUtil.myPostJson({
            url: `tborder/saveTbOrder`,
            data: tbOrder
        })
    }

    updateTbOrder(tbOrder) {
        return this.httpUtil.myPost({
            url: `tborder/update`,
            data: tbOrder
        })
    }

    /**
     * 更新订单是否可以操作
     * @param tbOrder
     */
    static updateEnableOperation(tbOrder) {
        let enable = false;
        if (tbOrder.saleMode === "0") {  // 物流订单
            if (tbOrder.status === "0") {
                enable = true;
            } else if (tbOrder.status === "1" && tbOrder.tbStore != null && tbOrder.tbStore.isSupportRefund) {
                enable = true;
            } else if (",3,4,".indexOf(tbOrder.status) !== -1 && !tbOrder.isComment) {
                enable = true;
            }
        } else if (tbOrder.saleMode === "1") {  // 消费券订单
            if (tbOrder.status === "0") {
                enable = true;
            } else if (tbOrder.status === "1" && tbOrder.tbStore != null && tbOrder.tbStore.isSupportRefund) {
                enable = true;
            } else if (tbOrder.status === "4" && !tbOrder.isComment) {
                enable = true;
            }
        } else if (tbOrder.saleMode === "2") {  // 餐饮订单
            if (tbOrder.status === "0") {
                enable = true;
            } else if (tbOrder.status === "1" && tbOrder.tbStore != null && tbOrder.tbStore.isSupportRefund) {
                enable = true;
            } else if (tbOrder.status === "4" && !tbOrder.isComment) {
                enable = true;
            }
        }
        tbOrder.enableOperation = enable;
    }

    batchDeleteTbOrderByIds(ids) {
        return this.httpUtil.myPost({
            url: `tborder/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }

    /**
     * 更新订单状态
     * @param tbOrder
     */
    updateStatus(tbOrder) {
        return this.httpUtil.myPost({
            url: `tborder/updateStatus`,
            data: tbOrder,
            isOptimizeData: true
        })
    }

    /**
     * 线下支付
     */
    offlinePay(tbOrder) {
        return this.httpUtil.myPost({
            url: `tborder/offlinePay`,
            data: tbOrder
        })
    }

    /**
     * 校验消费券号是否正确
     */
    validConsumerVoucher(tbOrder) {
        return this.httpUtil.myPost({
            url: `tborder/validConsumerVoucher`,
            data: tbOrder
        })
    }

    /**
     * 添加卖家备注
     */
    addSellerRemark(tbOrder) {
        return this.httpUtil.myPost({
            url: `tborder/addSellerRemark`,
            data: tbOrder
        })
    }

    /**
     * 卖家手动执行商品订单资金回笼
     */
    transferMoneyToSeller(tbOrder) {
        return this.httpUtil.myPost({
            url: `tborder/transferMoneyToSeller`,
            data: tbOrder
        })
    }

    /**
     * 更新收货地址
     */
    updateShippingAddress(tbOrder) {
        return this.httpUtil.myPost({
            url: `tborder/updateShippingAddress`,
            data: tbOrder
        })
    }

    /**
     * 更新物流信息
     */
    updateLogisticsInfo(tbOrder) {
        return this.httpUtil.myPost({
            url: `tborder/updateLogisticsInfo`,
            data: tbOrder
        })
    }
}
