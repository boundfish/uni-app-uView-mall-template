import {BaseService} from '../core/base/base.service'

export class TbLogisticsTracesService extends BaseService {

    findTbLogisticsTracesList(tbLogisticsTraces) {
        return this.httpUtil.myPost({
            url: `tblogisticstraces/findList`,
            data: tbLogisticsTraces
        })
    }

    findTbLogisticsTracesById(id) {
        return this.httpUtil.myPost({
            url: `tblogisticstraces/findById`,
            data: {id: id}
        })
    }

    saveTbLogisticsTraces(tbLogisticsTraces) {
        return this.httpUtil.myPost({
            url: `tblogisticstraces/save`,
            data: tbLogisticsTraces
        })
    }

    updateTbLogisticsTraces(tbLogisticsTraces) {
        return this.httpUtil.myPost({
            url: `tblogisticstraces/update`,
            data: tbLogisticsTraces
        })
    }

    batchDeleteTbLogisticsTracesByIds(ids) {
        return this.httpUtil.myPost({
            url: `tblogisticstraces/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }
}
