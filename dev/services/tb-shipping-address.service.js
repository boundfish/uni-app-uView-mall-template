import {BaseService} from '../core/base/base.service'

export class TbShippingAddressService extends BaseService {
  findTbShippingAddressList (tbShippingAddress) {
    return this.httpUtil.myPost({
      url: `tbshippingaddress/findList`,
      data: tbShippingAddress
    })
  }

  findTbShippingAddressById (id) {
    return this.httpUtil.myPost({
      url: `tbshippingaddress/findById`,
      data: {id: id}
    })
  }

  saveTbShippingAddress (tbShippingAddress) {
    return this.httpUtil.myPost({
      url: `tbshippingaddress/save`,
      data: tbShippingAddress
    })
  }

  updateTbShippingAddress (tbShippingAddress) {
    return this.httpUtil.myPost({
      url: `tbshippingaddress/update`,
      data: tbShippingAddress
    })
  }

  batchDeleteTbShippingAddressByIds (ids) {
    return this.httpUtil.myPost({
      url: `tbshippingaddress/deleteByIds`,
      data: {
        ids: ids
      }
    })
  }

  setDefaultTbShippingAddress (tbShippingAddress) {
    return this.httpUtil.myPost({
      url: `tbshippingaddress/setDefaultTbShippingAddress`,
      data: {
        id: tbShippingAddress.id,
        userId: this.globalVariable.userInfo.id
      }
    })
  }

  /**
   * 查询用户默认收货地址
   * @returns {any}
   */
  findDefaultTbShippingAddress () {
    return this.httpUtil.myPost({
      url: `tbshippingaddress/findDefaultTbShippingAddress`,
      data: {
        userId: this.globalVariable.userInfo.id,
        isDefault: true
      }
    })
  }
}
