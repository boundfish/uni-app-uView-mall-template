import {BaseService} from '../core/base/base.service'

export class TbGoodsAfterSaleService extends BaseService {

    findTbGoodsAfterSaleList(tbGoodsAfterSale) {
        return this.httpUtil.myPost({
            url: `tbgoodsaftersale/findList`,
            data: tbGoodsAfterSale
        })
    }

    saveTbGoodsAfterSale(tbGoodsAfterSale) {
        return this.httpUtil.myPost({
            url: `tbgoodsaftersale/save`,
            data: tbGoodsAfterSale
        })
    }

    updateTbGoodsAfterSale(tbGoodsAfterSale) {
        return this.httpUtil.myPost({
            url: `tbgoodsaftersale/update`,
            data: tbGoodsAfterSale
        })
    }

    batchDeleteTbGoodsAfterSaleByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbgoodsaftersale/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }
}
