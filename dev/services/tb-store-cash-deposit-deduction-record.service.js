import {BaseService} from '../core/base/base.service'

export class TbStoreCashDepositDeductionRecordService extends BaseService {

    findTbStoreCashDepositDeductionRecordList(tbStoreCashDepositDeductionRecord) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositdeductionrecord/findList`,
            data: tbStoreCashDepositDeductionRecord
        });
    }

    findTableList(params) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositdeductionrecord/findTableList?${params.pagination.appendToUrl()}`,
            data: params.data
        });
    }

    findTableTbStoreCashDepositDeductionRecordList(params) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositdeductionrecord/findTableTbStoreCashDepositDeductionRecordList?${params.pagination.appendToUrl()}`,
            data: params.data
        });
    }

    saveTbStoreCashDepositDeductionRecord(tbStoreCashDepositDeductionRecord) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositdeductionrecord/save`,
            data: tbStoreCashDepositDeductionRecord
        });
    }

    updateTbStoreCashDepositDeductionRecord(tbStoreCashDepositDeductionRecord) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositdeductionrecord/update`,
            data: tbStoreCashDepositDeductionRecord
        });
    }

    findTbStoreCashDepositDeductionRecordById(id) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositdeductionrecord/findById`,
            data: {id: id}
        });
    }

    findOne(tbStoreCashDepositDeductionRecord) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositdeductionrecord/findOne`,
            data: tbStoreCashDepositDeductionRecord
        });
    }
}
