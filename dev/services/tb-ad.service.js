import {BaseService} from '../core/base/base.service'
import {CommonUtil} from "../core/utils/common.util";

export class TbAdService extends BaseService {

    findTbAdList(tbAd) {
        return this.httpUtil.myPost({
            url: `tbad/findList`,
            data: tbAd,
        })
    }

    findTbAdListCascade(tbAd) {
        return this.httpUtil.myPost({
            url: `tbad/findTbAdListCascade`,
            data: tbAd,
        })
    }

    findTbAdById(id) {
        return this.httpUtil.myPost({
            url: `tbad/findById`,
            data: {id: id},
        })
    }

    saveTbAd(tbAd) {
        return this.httpUtil.myPost({
            url: `tbad/save`,
            data: tbAd
        })
    }

    updateTbAd(tbAd) {
        return this.httpUtil.myPost({
            url: `tbad/update`,
            data: tbAd
        })
    }

    deleteTbAdById(id) {
        return this.httpUtil.myPost({
            url: `tbad/deleteById`,
            data: {id: id}
        })
    }

    /**
     * 通过广告类型导航到对应页面
     * 广告类型：0-仅展示广告图片，1-内部打开网址，2-跳转商品详情，3-跳转店铺，4-跳转到一级系统分类，5-跳转到二级系统分类，6-跳转到商城动态分类列表，7-跳转到商城动态详情
     */
    navByType(_selfVue, tbAd) {
        if (tbAd.type === "0") {
        } else if (tbAd.type === "1") {
            CommonUtil.navigateTo(`/pages/common/open-outer-url/index?url=${tbAd.navUrl}`);
        } else if (tbAd.type === "2") {
            CommonUtil.navigateTo(`/pages/common/goods-detail/index?id=${tbAd.navGoodsId}`);
        } else if (tbAd.type === "3") {
            CommonUtil.navigateTo(`/pages/common/store/index?storeId=${tbAd.navStoreId}`);
        } else if (tbAd.type === "4") {
            _selfVue.switchTarBarByComponentRef('goodsClassRef');
        } else if (tbAd.type === "5") {
            let parent = {
                id: tbAd.navSystemClassGrade2ParentId,
                name: tbAd.navSystemClassGrade2ParentName,
                type: tbAd.navSystemClassGrade2ParentType,
            };
            let item = {
                id: tbAd.navSystemClassGrade2Id,
                type: tbAd.navSystemClassGrade2ParentType,
            };
            CommonUtil.navigateTo(`/pages/sub/sub1-tabs/tabs/goods-class/goods-class-filter/index`, null, {parentStoreClass: parent, tbStoreClass: item});
        } else if (tbAd.type === "6") {
            let tbCommunityDynamicType = {
                id: tbAd.navCommunityDynamicTypeId,
                name: tbAd.navCommunityDynamicTypeName
            };
            CommonUtil.navigateTo(`/pages/sub/sub1-tabs/tabs/community-dynamic/community-dynamic-list/index`, null, tbCommunityDynamicType);
        } else if (tbAd.type === "7") {
            let tbCommunityDynamic = {
                id: tbAd.navCommunityDynamicId
            };
            CommonUtil.navigateTo(`/pages/sub/sub1-tabs/tabs/community-dynamic/community-dynamic-list/community-dynamic-detail/index`, null, tbCommunityDynamic);
        }
    }
}
