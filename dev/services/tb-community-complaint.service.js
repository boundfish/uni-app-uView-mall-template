import {BaseService} from "../core/base/base.service";
import {CommonUtil} from "../core/utils/common.util";

export class TbCommunityComplaintService extends BaseService {

    findTbCommunityComplaintList(tbCommunityComplaint) {
        return this.httpUtil.myPost({
            url: `tbcommunitycomplaint/findList`,
            data: tbCommunityComplaint,
            orderBy: 'display_order asc'
        });
    }

    saveTbCommunityComplaint(tbCommunityComplaint) {
        return this.httpUtil.myPost({
            url: `tbcommunitycomplaint/save`,
            data: tbCommunityComplaint
        });
    }

    updateTbCommunityComplaint(tbCommunityComplaint) {
        return this.httpUtil.myPost({
            url: `tbcommunitycomplaint/update`,
            data: tbCommunityComplaint
        });
    }

    deleteTbCommunityComplaintByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbcommunitycomplaint/deleteByIds`,
            data: {ids: ids}
        });
    }

    /**
     * 举报留言
     */
    complaintAdd(item, type) {
        const _self = this;

        let data = {
            message: '',
            title: '',
        };
        if (type == 0) {
            data.message = '确定举报该动态吗？';
            data.title = '举报动态';
        } else if (type == 1) {
            data.message = '确定举报该留言吗？';
            data.title = '举报留言';
        } else if (type == 2) {
            data.message = '确定举报该评价吗？';
            data.title = '举报评价';
        } else if (type == 3) {
            data.message = '确定举报该店铺吗？';
            data.title = '举报店铺';
        }

        CommonUtil.alert({
            header: '提示！',
            message: data.message,
            buttons: [
                {
                    text: '取消',
                    role: 'cancel',
                    handler: (blah) => {
                    }
                },
                {
                    text: '确定',
                    handler: () => {
                        _self.router.navigate(['/community-complaint-add'], {
                            queryParams: {
                                title: data.title,
                                type: type,
                                fkId: item.id,
                                toUserId: item.userId,
                            }
                        });
                    }
                }
            ]
        });
    }
}
