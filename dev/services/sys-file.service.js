import {BaseService} from '../core/base/base.service'

export class SysFileService extends BaseService {

    deleteSysFileById(params) {
        const _self = this;
        _self.httpUtil.myGet({
            url: `sysfile/deleteById?id=${params.id}`
        })
    }

    deleteSysFile(sysFile) {
        return this.httpUtil.myPost({
            url: `sysfile/deleteById`,
            data: sysFile
        })
    }

    findSysFileList(params) {
        console.log("findSysFileList(params) {", params);
        return this.httpUtil.myGet({
            url: `sysfile/findList`,
            data: {
                id: params.id ? params.id : undefined,
                fkId: params.fkId ? params.fkId : undefined,
                code: params.code ? params.code : undefined,
                name: params.name ? params.name : undefined,
                type: params.type ? params.type : undefined,
                typeMark: params.typeMark ? params.typeMark : undefined,
                orderBy: 'display_order asc'
            }
        });
    }

    updateSysFile(sysFile) {
        return this.httpUtil.myPost({
            url: `sysfile/update`,
            data: sysFile
        })
    }

    saveSysFile(sysFile) {
        return this.httpUtil.myPost({
            url: `sysfile/save`,
            data: sysFile,
        })
    }

    batchSaveOrUpdateFile(json) {
        return this.httpUtil.myPost({
            url: `sysfile/batchSaveOrUpdate`,
            data: json,
        })
    }

    /**
     * 查找七牛云上的图片url
     * @param sysFile
     * @return result.url // 七牛云吐图片地址
     */
    findQiniuCloudImageUrl(sysFile) {
        return this.httpUtil.myPost({
            url: `sysfile/findQiniuCloudImageUrl`,
            data: sysFile,
        })
    }
}
