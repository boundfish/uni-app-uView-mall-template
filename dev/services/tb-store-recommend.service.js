import {BaseService} from '../core/base/base.service'

export class TbStoreRecommendService extends BaseService {

    findTbStoreRecommendList(tbStoreRecommend) {
        return this.httpUtil.myPost({
            url: `tbstorerecommend/findList`,
            data: tbStoreRecommend,
        })
    }

    findTbStoreRecommendListCascade(tbStoreRecommend) {
        return this.httpUtil.myPost({
            url: `tbstorerecommend/findTbStoreRecommendListCascade`,
            data: tbStoreRecommend,
        })
    }

    findTbStoreRecommendById(id) {
        return this.httpUtil.myPost({
            url: `tbstorerecommend/findById`,
            data: {id: id},
        })
    }

    saveTbStoreRecommend(tbStoreRecommend) {
        return this.httpUtil.myPost({
            url: `tbstorerecommend/save`,
            data: tbStoreRecommend
        })
    }

    updateTbStoreRecommend(tbStoreRecommend) {
        return this.httpUtil.myPost({
            url: `tbstorerecommend/update`,
            data: tbStoreRecommend
        })
    }

    deleteTbStoreRecommendById(id) {
        return this.httpUtil.myPost({
            url: `tbstorerecommend/deleteById`,
            data: {id: id}
        })
    }
}
