import {BaseService} from '../core/base/base.service'

export class TbCollectUserStoreService extends BaseService {

    findTbCollectUserStoreList(tbCollectUserStore) {
        return this.httpUtil.myPost({
            url: `tbcollectuserstore/findList`,
            data: tbCollectUserStore
        })
    }

    findTableListCascade(params) {
        return this.httpUtil.myPost({
            url: `tbcollectuserstore/findTableListCascade?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    saveTbCollectUserStore(tbCollectUserStore) {
        return this.httpUtil.myPost({
            url: `tbcollectuserstore/save`,
            data: tbCollectUserStore
        })
    }

    updateTbCollectUserStore(tbCollectUserStore) {
        return this.httpUtil.myPost({
            url: `tbcollectuserstore/update`,
            data: tbCollectUserStore
        })
    }

    deleteTbCollectUserStore(tbCollectUserStore) {
        return this.httpUtil.myPost({
            url: `tbcollectuserstore/deleteById`,
            data: tbCollectUserStore
        })
    }

    batchDeleteTbCollectUserStoreByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbcollectuserstore/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }

    findCollectNum(tbCollectUserStore) {
        return this.httpUtil.myPost({
            url: `tbcollectuserstore/findCollectNum`,
            data: tbCollectUserStore
        })
    }

    isCollectedStore(tbCollectUserStore) {
        return this.httpUtil.myPost({
            url: `tbcollectuserstore/isCollected`,
            data: tbCollectUserStore
        });
    }
}
