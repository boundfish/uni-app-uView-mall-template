import {BaseService} from "../core/base/base.service";

export class TbCommunityAdminService extends BaseService {

    findTbCommunityAdminList(tbCommunityAdmin) {
        return this.httpUtil.myPost({
            url: `tbcommunityadmin/findList`,
            data: tbCommunityAdmin,
            orderBy: "display_order asc"
        });
    }

    findTbCommunityAdminById(id) {
        return this.httpUtil.myPost({
            url: `tbcommunityadmin/findById`,
            data: {id: id},
        });
    }

    findTableTbCommunityAdminList(tbCommunityAdmin) {
        return this.httpUtil.myPost({
            url: `tbcommunityadmin/findTableList`,
            data: tbCommunityAdmin,
            orderBy: "display_order asc"
        });
    }

    saveTbCommunityAdmin(tbCommunityAdmin) {
        return this.httpUtil.myPost({
            url: `tbcommunityadmin/save`,
            data: tbCommunityAdmin
        });
    }

    updateTbCommunityAdmin(tbCommunityAdmin) {
        return this.httpUtil.myPost({
            url: `tbcommunityadmin/update`,
            data: tbCommunityAdmin
        });
    }

    deleteTbCommunityAdminByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbcommunityadmin/deleteByIds`,
            data: {ids: ids}
        });
    }
}
