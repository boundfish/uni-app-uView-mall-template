import {BaseService} from "../core/base/base.service";

export class TbCommunityDynamicCommentService extends BaseService {

    findTree(tbCommunityDynamicComment) {
        return this.httpUtil.myPost({
                url: `tbcommunitydynamiccomment/findTree`,
                data: tbCommunityDynamicComment
        });
    }

    findTbCommunityDynamicCommentList(tbCommunityDynamicComment) {
        return this.httpUtil.myPost({
                url: `tbcommunitydynamiccomment/findList`,
                data: tbCommunityDynamicComment
        });
    }

    findTableTbCommunityDynamicCommentList(params) {
        return this.httpUtil.myPost({
                url: `tbcommunitydynamiccomment/findTableListCascade?${params.pagination.appendToUrl()}`,
                data: params.data
        });
    }

    findReplyTableListCascade(params) {
        return this.httpUtil.myPost({
                url: `tbcommunitydynamiccomment/findReplyTableListCascade?${params.pagination.appendToUrl()}`,
                data: params.data,
        });
    }

    findTbCommunityDynamicCommentById(id) {
        return this.httpUtil.myPost({
                url: `tbcommunitydynamiccomment/findTbCommunityDynamicCommentById`,
                data: {id: id}
        });
    }

    saveTbCommunityDynamicComment(tbCommunityDynamicComment) {
        return this.httpUtil.myPost({
                url: `tbcommunitydynamiccomment/save`,
                data: tbCommunityDynamicComment
        });
    }

    updateTbCommunityDynamicComment(tbCommunityDynamicComment) {
        return this.httpUtil.myPost({
                url: `tbcommunitydynamiccomment/update`,
                data: tbCommunityDynamicComment
        });
    }

    batchDeleteTbCommunityDynamicCommentByIds(ids) {
        return this.httpUtil.myPost({
                url: `tbcommunitydynamiccomment/deleteByIds`,
                data: {
                    ids: ids
                }
        });
    }
}
