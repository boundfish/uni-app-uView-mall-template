import {BaseService} from '../core/base/base.service'

export class SysDictionaryService extends BaseService {

    /**
     * 获取下拉控件值列表
     * @param parentCode  // 数据字典编码，根据该编码查询所有下级节点
     * @param excludeSubCods // 过滤掉不需要的数据字典编号，过滤多个值以逗号,分割。
     */
    optionList(parentCode, excludeSubCods = null) {
        return this.httpUtil.myPost({
            url: `sysdictionary/findSysDictionaryListByParentCode`,
            data: {
                code: parentCode,
                excludeCods: excludeSubCods
            }
        })
    }

    findSysDictionaryTree(sysDictionary) {
        return this.httpUtil.myPost({
            url: `sysdictionary/findTree`,
            data: sysDictionary
        })
    }

    saveSysDictionary(sysDictionary) {
        return this.httpUtil.myPost({
            url: `sysdictionary/save`,
            data: sysDictionary
        })
    }

    updateSysDictionary(sysDictionary) {
        return this.httpUtil.myPost({
            url: `sysdictionary/update`,
            data: sysDictionary
        })
    }

    deleteSysDictionary(sysDictionary) {
        return this.httpUtil.myPost({
            url: `sysdictionary/deleteByIdCascade`,
            data: {id: sysDictionary.id}
        })
    }
}
