import {BaseService} from '../core/base/base.service'

export class TbCommunityUserTypeService extends BaseService {

    findTableTbCommunityUserTypeList(params) {
        return this.httpUtil.myPost({
            url: `tbcommunityusertype/findTableTbCommunityUserTypeList?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTbCommunityUserTypeList(tbCommunityUserType) {
        return this.httpUtil.myPost({
            url: `tbcommunityusertype/findList`,
            data: tbCommunityUserType
        })
    }

    saveTbCommunityUserType(tbCommunityUserType) {
        return this.httpUtil.myPost({
            url: `tbcommunityusertype/save`,
            data: tbCommunityUserType
        })
    }

    updateTbCommunityUserType(tbCommunityUserType) {
        return this.httpUtil.myPost({
            url: `tbcommunityusertype/update`,
            data: tbCommunityUserType
        })
    }

    batchDeleteTbCommunityUserTypeByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbcommunityusertype/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }
}
