import {BaseService} from "../core/base/base.service";

export class OrderGoodsAfterSaleReportService extends BaseService {

    statisticOrderGoodsAfterSaleTotal(params) {
        return this.httpUtil.myPost({
            url: `ordergoodaftersalesreport/statisticOrderGoodsAfterSaleTotal`,
            data: params
        });
    }
}
