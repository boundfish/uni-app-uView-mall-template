import {BaseService} from "../core/base/base.service";

export class TbFeedbackService extends BaseService {

    findTbFeedbackTableList(params) {
        return this.httpUtil.myPost({
            url: `tbfeedback/findTableList?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTableListByCascade(params) {
        return this.httpUtil.myPost({
            url: `tbfeedback/findTableListByCascade?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTbFeedbackList(tbFeedback) {
        return this.httpUtil.myPost({
            url: `tbfeedback/findList`,
            data: tbFeedback,
            orderBy: 'display_order asc'
        });
    }

    saveTbFeedback(tbFeedback) {
        return this.httpUtil.myPost({
            url: `tbfeedback/save`,
            data: tbFeedback
        });
    }

    updateTbFeedback(tbFeedback) {
        const _self = this;
        return this.httpUtil.myPost({
            url: `tbfeedback/update`,
            data: tbFeedback
        });
    }

    deleteTbFeedbackByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbfeedback/deleteByIds`,
            data: {ids: ids}
        });
    }
}
