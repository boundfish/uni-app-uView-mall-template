import {BaseService} from '../core/base/base.service'

export class TbStorePresentService extends BaseService {

    findTbStorePresentList(tbStorePresent) {
        return this.httpUtil.myPost({
            url: `tbstorepresent/findList`,
            data: tbStorePresent,
        })
    }

    saveTbStorePresent(tbStorePresent) {
        return this.httpUtil.myPost({
            url: `tbstorepresent/save`,
            data: tbStorePresent
        })
    }

    updateTbStorePresent(tbStorePresent) {
        return this.httpUtil.myPost({
            url: `tbstorepresent/update`,
            data: tbStorePresent
        })
    }

    deleteTbStorePresentByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbstorepresent/deleteByIds`,
            data: {ids: ids}
        })
    }

    deleteTbStorePresent(tbStorePresent) {
        return this.httpUtil.myPost({
            url: `tbstorepresent/deleteByIdCascade`,
            data: {id: tbStorePresent.id}
        })
    }
}
