import {BaseService} from '../core/base/base.service'

export class SysOrganizationService extends BaseService {

  findSysOrganizationTree (sysOrganization) {
    return this.httpUtil.myPost({
      url: `sysorganization/findTree`,
      data: sysOrganization
    })
  }

  saveSysOrganization (sysOrganization) {
    return this.httpUtil.myPost({
      url: `sysorganization/save`,
      data: sysOrganization
    })
  }

  updateSysOrganization (sysOrganization) {
    return this.httpUtil.myPost({
      url: `sysorganization/update`,
      data: sysOrganization
    })
  }

  deleteSysOrganization (sysOrganization) {
    return this.httpUtil.myPost({
      url: `sysorganization/deleteByIdCascade`,
      data: {id: sysOrganization.id}
    })
  }

  findSysOrganizationListByUserId (data) {
    return this.httpUtil.myPost({
      url: `sysorganization/findSysOrganizationListByUserId`,
      data: data
    })
  }
}
