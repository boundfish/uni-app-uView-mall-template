import {BaseService} from '../core/base/base.service'

export class TbCommunityService extends BaseService {

    findTbCommunityList(tbCommunity) {
        return this.httpUtil.myPost({
            url: `tbcommunity/findList`,
            data: tbCommunity,
            orderBy: 'display_order asc'
        })
    }

    findTbCommunityById(id) {
        return this.httpUtil.myPost({
            url: `tbcommunity/findById`,
            data: {id: id},
        })
    }

    findTOne(tbCommunity) {
        return this.httpUtil.myPost({
            url: `tbcommunity/findTOne`,
            data: tbCommunity,
        })
    }

    saveTbCommunityAndSendMail(tbCommunity) {
        return this.httpUtil.myPost({
            url: `tbcommunity/saveTbCommunityAndSendMail`,
            data: tbCommunity
        })
    }

    /**
     * 开通新商城
     * @param tbCommunity
     * @returns {*}
     */
    openNewTbCommunity(tbCommunity) {
        return this.httpUtil.myPost({
            url: `tbcommunity/openNewTbCommunity`,
            data: tbCommunity
        })
    }

    updateTbCommunity(tbCommunity) {
        return this.httpUtil.myPost({
            url: `tbcommunity/update`,
            data: tbCommunity
        })
    }

    deleteTbCommunityByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbcommunity/deleteByIds`,
            data: {ids: ids}
        })
    }

    findTableCommunityMemberList(params) {
        return this.httpUtil.myPost({
            url: `tbcommunity/findTableCommunityMemberList?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }
}
