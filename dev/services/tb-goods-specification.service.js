import {BaseService} from '../core/base/base.service'

export class TbGoodsSpecificationService extends BaseService {

    findListCascade(tbGoodsSpecification) {
        return this.httpUtil.myPost({
            url: `tbgoodsspecification/findTableListCascade?pageNum=1&pageSize=9999`,
            data: tbGoodsSpecification
        })
    }

    findTbGoodsSpecificationList(tbGoodsSpecification) {
        return this.httpUtil.myPost({
            url: `tbgoodsspecification/findList`,
            data: tbGoodsSpecification
        })
    }

    saveTbGoodsSpecification(tbGoodsSpecification) {
        return this.httpUtil.myPostJson({
            url: `tbgoodsspecification/saveTbGoodsSpecification`,
            data: tbGoodsSpecification
        })
    }

    updateTbGoodsSpecification(tbGoodsSpecification) {
        return this.httpUtil.myPostJson({
            url: `tbgoodsspecification/updateTbGoodsSpecification`,
            data: tbGoodsSpecification
        })
    }

    batchDeleteTbGoodsSpecificationByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbgoodsspecification/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }
}
