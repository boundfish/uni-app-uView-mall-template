import {BaseService} from "../core/base/base.service";

export class TbCommunityDynamicReadedService extends BaseService {

    findTbCommunityDynamicReadedList(tbCommunityDynamicReaded) {
        return this.httpUtil.myPost({
            url: `tbcommunitydynamicreaded/findList`,
            data: tbCommunityDynamicReaded,
            orderBy: 'display_order asc'
        });
    }

    findUnreadCount(tbCommunityDynamicReaded) {
        return this.httpUtil.myPost({
            url: `tbcommunitydynamicreaded/findUnreadCount`,
            data: tbCommunityDynamicReaded,
        });
    }

    saveTbCommunityDynamicReaded(tbCommunityDynamicReaded) {
        return this.httpUtil.myPost({
            url: `tbcommunitydynamicreaded/saveTbCommunityDynamicReaded`,
            data: tbCommunityDynamicReaded
        });
    }

    updateTbCommunityDynamicReaded(tbCommunityDynamicReaded) {
        return this.httpUtil.myPost({
            url: `tbcommunitydynamicreaded/update`,
            data: tbCommunityDynamicReaded
        });
    }

    deleteTbCommunityDynamicReadedByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbcommunitydynamicreaded/deleteByIds`,
            data: {ids: ids}
        });
    }
}
