import {BaseService} from '../core/base/base.service'

export class SysAdministrativeRegionService extends BaseService {

  findSysAdministrativeRegionCascaderList () {
    return this.httpUtil.myPost({
      url: `sysadministrativeregion/findCascaderList`
    })
  }
}
