import {BaseService} from '../core/base/base.service'

export class SysResourceService extends BaseService {
  findSysResourceTree (sysDictionary) {
    return this.httpUtil.myPost({
      url: `sysresource/findTree`,
      data: sysDictionary
    })
  }

  saveSysResource (sysDictionary) {
    return this.httpUtil.myPost({
      url: `sysresource/save`,
      data: sysDictionary
    })
  }

  updateSysResource (sysDictionary) {
    return this.httpUtil.myPost({
      url: `sysresource/update`,
      data: sysDictionary
    })
  }

  deleteSysResource (sysDictionary) {
    return this.httpUtil.myPost({
      url: `sysresource/deleteByIdCascade`,
      data: {id: sysDictionary.id}
    })
  }

  findAssignedResourcesIdsByRoleId (data) {
    return this.httpUtil.myPost({
      url: `sysresource/findAssignedResourcesIdsByRoleId`,
      data: data
    })
  }

  saveAssignedResourcesByRoleId (data) {
    return this.httpUtil.myPost({
      url: `sysresource/saveAssignedResourcesByRoleId`,
      data: data
    })
  }
}
