import {BaseService} from '../core/base/base.service'

export class TbStoreGoodsRecommendTemplateService extends BaseService {

    findListCascade(tbStoreAdTemplate) {
        return this.httpUtil.myPost({
            url: `tbstoregoodsrecommendtemplate/findListCascade`,
            data: tbStoreAdTemplate
        })
    }

    findTbStoreGoodsRecommendTemplateList(tbStoreGoodsRecommendTemplate) {
        return this.httpUtil.myPost({
            url: `tbstoregoodsrecommendtemplate/findList`,
            data: tbStoreGoodsRecommendTemplate
        })
    }

    saveTbStoreGoodsRecommendTemplate(tbStoreGoodsRecommendTemplate) {
        return this.httpUtil.myPost({
            url: `tbstoregoodsrecommendtemplate/save`,
            data: tbStoreGoodsRecommendTemplate
        })
    }

    updateAssociatedStore(tbStoreAdTemplate) {
        return this.httpUtil.myPost({
            url: `tbstoregoodsrecommendtemplate/updateAssociatedStore`,
            data: tbStoreAdTemplate
        })
    }

    updateTbStoreGoodsRecommendTemplate(tbStoreGoodsRecommendTemplate) {
        return this.httpUtil.myPost({
            url: `tbstoregoodsrecommendtemplate/update`,
            data: tbStoreGoodsRecommendTemplate
        })
    }

    batchDeleteTbStoreGoodsRecommendTemplateByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbstoregoodsrecommendtemplate/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }
}
