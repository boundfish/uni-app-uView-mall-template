import {BaseService} from '../core/base/base.service'

export class TbGoodsDetailAdTemplateService extends BaseService {

    findTableTbGoodsListCascade(params) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailadtemplate/findTableTbGoodsListCascade?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTbGoodsDetailAdTemplateList(tbGoodsDetailAdTemplate) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailadtemplate/findList`,
            data: tbGoodsDetailAdTemplate
        })
    }

    findTransferTbGoodsDetailAdTemplateList(tbGoodsDetailAdTemplate) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailadtemplate/findTransferTbGoodsDetailAdTemplateList`,
            data: tbGoodsDetailAdTemplate
        })
    }

    saveTbGoodsDetailAdTemplate(tbGoodsDetailAdTemplate) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailadtemplate/save`,
            data: tbGoodsDetailAdTemplate
        })
    }

    batchSaveAssignedTbGoodsDetailAdTemplates(tbGoodsDetailAdTemplate) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailadtemplate/batchSaveAssignedTbGoodsDetailAdTemplates`,
            data: tbGoodsDetailAdTemplate
        })
    }

    updateTbGoodsDetailAdTemplate(tbGoodsDetailAdTemplate) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailadtemplate/update`,
            data: tbGoodsDetailAdTemplate
        })
    }

    batchDeleteTbGoodsDetailAdTemplateByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailadtemplate/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }

    batchDelAssignedTbGoodsDetailAdTemplates(goodsIds) {
        return this.httpUtil.myPost({
            url: `tbgoodsdetailadtemplate/batchDelAssignedTbGoodsDetailAdTemplates`,
            data: {goodsIds: goodsIds}
        })
    }
}
