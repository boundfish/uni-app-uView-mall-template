import {BaseService} from "../core/base/base.service";

export class StoreReportService extends BaseService {

    statisticStoreInfo(params) {
        return this.httpUtil.myPost({
            url: `storereport/statisticStoreInfo`,
            data: params
        });
    }
}
