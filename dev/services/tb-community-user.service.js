import {BaseService} from '../core/base/base.service'

export class TbCommunityUserService extends BaseService {


    findTbCommunityUserById(id) {
        return this.httpUtil.myPost({
            url: `tbcommunityuser/findTbCommunityUserById`,
            data: {id: id},
        })
    }

    findTbCommunityUserList(tbCommunityUser) {
        return this.httpUtil.myPost({
            url: `tbcommunityuser/findList`,
            data: tbCommunityUser,
            orderBy: 'display_order asc'
        })
    }

    findTableList(params) {
        return this.httpUtil.myPost({
            url: `tbcommunityuser/findTableList?${params.pagination.appendToUrl()}`,
            data: params.data,
        })
    }

    findTableTbCommunityUserList(params) {
        return this.httpUtil.myPost({
            url: `tbcommunityuser/findTableTbCommunityUserList?${params.pagination.appendToUrl()}`,
            data: params.data,
        })
    }

    findTableListCascade(params) {
        return this.httpUtil.myPost({
            url: `tbcommunityuser/findTableListCascade?${params.pagination.appendToUrl()}`,
            data: params.data,
        })
    }

    isCanAssociation(tbCommunityUser) {
        return this.httpUtil.myPost({
            url: `tbcommunityuser/isCanAssociation`,
            data: tbCommunityUser
        });
    }

    /**
     * 查找业务员跟进的客户列表
     * @param params
     * @returns {*}
     */
    findTableTbCommunityUserListBySalesman(params) {
        return this.httpUtil.myPost({
            url: `tbcommunityuser/findTableTbCommunityUserListBySalesman?${params.pagination.appendToUrl()}`,
            data: params.data,
        })
    }

    saveTbCommunityUser(tbCommunityUser) {
        return this.httpUtil.myPost({
            url: `tbcommunityuser/save`,
            data: tbCommunityUser
        })
    }

    updateTbCommunityUser(tbCommunityUser) {
        return this.httpUtil.myPost({
            url: `tbcommunityuser/update`,
            data: tbCommunityUser
        })
    }

    deleteTbCommunityUserByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbcommunityuser/deleteByIds`,
            data: {ids: ids}
        })
    }

    /**
     * 设置默认登录的商城
     * @param tbCommunityUser
     */
    setDefaultTbCommunityUser(tbCommunityUser) {
        return this.httpUtil.myPost({
            url: `tbcommunityuser/setDefaultTbCommunityUser`,
            data: tbCommunityUser
        })
    }

    /**
     * 校验扫码结果是否正确
     */
    verifyQRCode(qrCode) {
        return this.httpUtil.myPost({
            url: `tbcommunityuser/verifyQRCode`,
            data: {qrCode: qrCode}
        })
    }

    /**
     * 访问该商城，并且将状态切换到商城
     */
    visitTbCommunity(tbCommunityUser) {
        return this.httpUtil.myPost({
            url: `tbcommunityuser/visitTbCommunity`,
            data: tbCommunityUser
        })
    }
}
