import {BaseService} from "../core/base/base.service";

export class OrderReportService extends BaseService {

    statisticOrderTotal(params) {
        return this.httpUtil.myPost({
            url: `orderreport/statisticOrderTotal`,
            data: params
        });
    }

    statisticOrderNumber(params) {
        return this.httpUtil.myPost({
            url: `orderreport/statisticOrderNumber`,
            data: params
        });
    }

    statisticOrderSaleMoney(params) {
        return this.httpUtil.myPost({
            url: `orderreport/statisticOrderSaleMoney`,
            data: params
        });
    }

    statisticOrderNumber24Hours(params) {
        return this.httpUtil.myPost({
            url: `orderreport/statisticOrderNumber24Hours`,
            data: params
        });
    }

    statisticOrderSaleMoney24Hours(params) {
        return this.httpUtil.myPost({
            url: `orderreport/statisticOrderSaleMoney24Hours`,
            data: params
        });
    }

    statisticOrderNumberRecent30Days(params) {
        return this.httpUtil.myPost({
            url: `orderreport/statisticOrderNumberRecent30Days`,
            data: params
        });
    }

    statisticOrderSaleMoneyRecent30Days(params) {
        return this.httpUtil.myPost({
            url: `orderreport/statisticOrderSaleMoneyRecent30Days`,
            data: params
        });
    }

    statisticOrderNumberRecentMonthsOfYear(params) {
        return this.httpUtil.myPost({
            url: `orderreport/statisticOrderNumberRecentMonthsOfYear`,
            data: params
        });
    }

    statisticOrderSaleMoneyRecentMonthsOfYear(params) {
        return this.httpUtil.myPost({
            url: `orderreport/statisticOrderSaleMoneyRecentMonthsOfYear`,
            data: params
        });
    }
}
