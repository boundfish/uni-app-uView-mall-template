import {BaseService} from "../core/base/base.service";

export class TbDistributorMoneyService extends BaseService {

    findTableListCascade(params) {
        return this.httpUtil.myPost({
            url: `tbdistributormoney/findTableListCascade?${params.pagination.appendToUrl()}`,
            data: params.data
        });
    }

    statisticTotal(tbDistributorMoney) {
        return this.httpUtil.myPost({
            url: `tbdistributormoney/statisticTotal`,
            data: tbDistributorMoney,
        });
    }

    withdrawalToAccount(tbDistributorMoney) {
        return this.httpUtil.myPost({
            url: `tbdistributormoney/withdrawalToAccount`,
            data: tbDistributorMoney,
        });
    }

    findTbDistributorMoneyList(tbDistributorMoney) {
        return this.httpUtil.myPost({
            url: `tbdistributormoney/findList`,
            data: tbDistributorMoney,
            orderBy: 'display_order asc'
        });
    }

    saveTbDistributorMoney(tbDistributorMoney) {
        return this.httpUtil.myPost({
            url: `tbdistributormoney/save`,
            data: tbDistributorMoney
        });
    }

    updateTbDistributorMoney(tbDistributorMoney) {
        return this.httpUtil.myPost({
            url: `tbdistributormoney/update`,
            data: tbDistributorMoney
        });
    }

    deleteTbDistributorMoneyByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbdistributormoney/deleteByIds`,
            data: {ids: ids}
        });
    }
}
