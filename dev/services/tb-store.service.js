import {BaseService} from '../core/base/base.service'

export class TbStoreService extends BaseService {

    findTbStoreList(tbStore) {
        return this.httpUtil.myPost({
            url: `tbstore/findList`,
            data: tbStore
        })
    }

    findTableList(params) {
        return this.httpUtil.myPost({
            url: `tbstore/findTableList?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTableListCascade(params) {
        return this.httpUtil.myPost({
            url: `tbstore/findTableListCascade?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTableTbStoreList(params) {
        return this.httpUtil.myPost({
            url: `tbstore/findTableTbStoreList?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTableTbStoreListByConsumer(params) {
        return this.httpUtil.myPost({
            url: `tbstore/findTableTbStoreListByConsumer?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    saveTbStore(tbStore) {
        return this.httpUtil.myPost({
            url: `tbstore/save`,
            data: tbStore
        })
    }

    updateTbStore(tbStore) {
        return this.httpUtil.myPost({
            url: `tbstore/update`,
            data: tbStore
        })
    }

    findTbStoreById(id) {
        return this.httpUtil.myPost({
            url: `tbstore/findById`,
            data: {id: id}
        })
    }

    findTbStoreByUserId(userId) {
        return this.httpUtil.myPost({
            url: `tbstore/findTbStoreByUserId`,
            data: {userId: userId}
        })
    }

    findOne(tbStore) {
        return this.httpUtil.myPost({
            url: `tbstore/findOne`,
            data: tbStore
        })
    }
}
