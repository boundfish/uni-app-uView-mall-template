import {BaseService} from '../core/base/base.service'

export class SysOrganizationUserService extends BaseService {
  batchSaveSysOrganizationUser (data) {
    return this.httpUtil.myPostJson({
      url: `sysorganizationuser/batchSave`,
      data: data
    })
  }

  batchDeleteSysOrganizationUser (data) {
    return this.httpUtil.myPostJson({
      url: `sysorganizationuser/batchDelete`,
      data: data
    })
  }

  updateSysOrganizationUser (sysOrganization) {
    return this.httpUtil.myPost({
      url: `sysorganizationuser/update`,
      data: sysOrganization
    })
  }
}
