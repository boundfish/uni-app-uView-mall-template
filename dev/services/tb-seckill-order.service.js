import {BaseService} from '../core/base/base.service'

export class TbSeckillOrderService extends BaseService {

    findTableListCascade(params) {
        return this.httpUtil.myPost({
            url: `tbseckillorder/findTableListCascade?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTbSeckillOrderList(tbSeckillOrder) {
        return this.httpUtil.myPost({
            url: `tbseckillorder/findListCascade`,
            data: tbSeckillOrder
        })
    }

    findTbSeckillOrderById(id) {
        return this.httpUtil.myPost({
            url: `tbseckillorder/findById`,
            data: {id: id}
        })
    }

    findCount(tbOrder) {
        return this.httpUtil.myPost({
            url: `tbseckillorder/findCount`,
            data: tbOrder
        })
    }

    saveTbSeckillOrder(tbSeckillOrder) {
        return this.httpUtil.myPostJson({
            url: `tbseckillorder/saveTbOrder`,
            data: tbSeckillOrder
        })
    }

    updateTbSeckillOrder(tbSeckillOrder) {
        return this.httpUtil.myPost({
            url: `tbseckillorder/update`,
            data: tbSeckillOrder
        })
    }

    batchDeleteTbSeckillOrderByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbseckillorder/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }
}
