import {BaseService} from '../core/base/base.service'

export class CommonService extends BaseService {
    /**
     * 唯一性检查
     */
    uniqueCheck(tableName, fieldName, fieldValue, excludeId) {
        return this.httpUtil.myPost({
            url: `common/uniqueCheck`,
            data: {
                tableName: tableName,
                fieldName: fieldName,
                fieldValue: fieldValue,
                excludeId: excludeId
            }
        })
    }

    /**
     * 多字段唯一性检查
     *
     *     {
     *        "tableName": "sys_user",
     *        "fieldList": [
     *            {"fieldName": "code11", "fieldValue": "111"},
     *            {"fieldName": "code", "fieldValue": "1"}
     *        ],
     *        "excludeId": ""
     *    }
     */
    uniqueCheckByFields(parameterMap) {
        return this.httpUtil.myPostJson({
            url: `common/uniqueCheckByFields`,
            data: parameterMap
        })
    }

    /**
     * 生成分享图片
     * @param storeId
     */
    generateShareImage(data) {
        return this.httpUtil.myPost({
            url: `common/generateShareImage`,
            data: data
        })
    }

    /**
     * web端测试
     */
    downloadApp() {
        return this.httpUtil.myGet({ // 获得服务器更新信息
            url: 'tbappversioninfo/selectNewest',
            data: {
                platformType: 'android',
                versionType: 'release',
                deleted: 0
            }
        })
    }

    /**
     * 后台生成二维码，自定义二维码内容。
     * @param content
     */
    generateQrcodeBase64(content) {
        return this.httpUtil.myGet({ // 获得服务器更新信息
            url: 'common/generateQrcodeToBase64',
            data: {
                content: content
            }
        })
    }

    /**
     * 获取七牛云上传Token。
     */
    findQiNiuUploadToken() {
        return this.httpUtil.myGet({  // 获得服务器更新信息
            url: 'common/findQiNiuUploadToken',
        })
    }
}
