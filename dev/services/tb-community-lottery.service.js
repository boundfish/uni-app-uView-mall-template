import {BaseService} from "../core/base/base.service";

export class TbCommunityLotteryService extends BaseService {

    findTbCommunityLotteryList(tbCommunityLottery) {
        return this.httpUtil.myPost({
            url: `tbcommunitylottery/findList`,
            data: tbCommunityLottery,
        });
    }

    findPreviousTableTbCommunityLotteryList(params) {
        return this.httpUtil.myPost({
            url: `tbcommunitylottery/findPreviousTableTbCommunityLotteryList?${params.pagination.appendToUrl()}`,
            data: params.data
        });
    }

    findNextTbCommunityLottery(tbCommunityLottery) {
        return this.httpUtil.myPost({
            url: `tbcommunitylottery/findNextTbCommunityLottery`,
            data: tbCommunityLottery,
        });
    }

    findTbCommunityLotteryById(id) {
        return this.httpUtil.myPost({
            url: `tbcommunitylottery/findById`,
            data: {id: id},
        });
    }

    findTbCommunityLotteryByWinningUser(tbCommunityLottery) {
        return this.httpUtil.myPost({
            url: `tbcommunitylottery/findTbCommunityLotteryByWinningUser`,
            data: tbCommunityLottery,
        });
    }

    /**
     * 查找我的中奖纪录
     */
    findTableTbCommunityLotteryListByMyWinning(params) {
        return this.httpUtil.myPost({
            url: `tbcommunitylottery/findTableTbCommunityLotteryListByMyWinning?${params.pagination.appendToUrl()}`,
            data: params.data,
        });
    }

    saveTbCommunityLottery(tbCommunityLottery) {
        return this.httpUtil.myPost({
            url: `tbcommunitylottery/save`,
            data: tbCommunityLottery
        });
    }

    updateTbCommunityLottery(tbCommunityLottery) {
        return this.httpUtil.myPost({
            url: `tbcommunitylottery/update`,
            data: tbCommunityLottery
        });
    }

    deleteTbCommunityLotteryById(id) {
        return this.httpUtil.myPost({
            url: `tbcommunitylottery/deleteById`,
            data: {id: id}
        });
    }
}
