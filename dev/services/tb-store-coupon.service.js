import {BaseService} from '../core/base/base.service'

export class TbStoreCouponService extends BaseService {
  findTbStoreCouponList (tbStoreCoupon) {
    return this.httpUtil.myPost({
      url: `tbstorecoupon/findList`,
      data: tbStoreCoupon,
      orderBy: 'display_order asc'
    })
  }

  findValidTbStoreCouponList (tbStoreCoupon) {
    return this.httpUtil.myPost({
      url: `tbstorecoupon/findValidTbStoreCouponList`,
      data: tbStoreCoupon,
      orderBy: 'display_order asc'
    })
  }

  validCouponCount (tbStoreCoupon) {
    return this.httpUtil.myPost({
      url: `tbstorecoupon/validCouponCount`,
      data: tbStoreCoupon
    })
  }

  saveTbStoreCoupon (tbStoreCoupon) {
    return this.httpUtil.myPost({
      url: `tbstorecoupon/save`,
      data: tbStoreCoupon
    })
  }

  updateTbStoreCoupon (tbStoreCoupon) {
    return this.httpUtil.myPost({
      url: `tbstorecoupon/update`,
      data: tbStoreCoupon
    })
  }

  deleteTbStoreCouponByIds (ids) {
    return this.httpUtil.myPost({
      url: `tbstorecoupon/deleteByIds`,
      data: {ids: ids}
    })
  }
}
