import {BaseService} from '../core/base/base.service'

export class TbWxSubscribeMessageService extends BaseService {

    findTbWxSubscribeMessageList(tbWxSubscribeMessage) {
        return this.httpUtil.myPost({
            url: `tbwxsubscribemessage/findList`,
            data: tbWxSubscribeMessage,
        })
    }

    findTbWxSubscribeMessageById(id) {
        return this.httpUtil.myPost({
            url: `tbwxsubscribemessage/findById`,
            data: {id: id},
        })
    }

    /**
     * 获取订阅数量
     * @param tbWxSubscribeMessage
     * @returns {*}
     */
    findTbWxSubscribeMessageCount(tbWxSubscribeMessage) {
        return this.httpUtil.myPost({
            url: `tbwxsubscribemessage/findCount`,
            data: tbWxSubscribeMessage,
        })
    }

    saveTbWxSubscribeMessage(tbWxSubscribeMessage) {
        return this.httpUtil.myPost({
            url: `tbwxsubscribemessage/save`,
            data: tbWxSubscribeMessage
        })
    }

    updateTbWxSubscribeMessage(tbWxSubscribeMessage) {
        return this.httpUtil.myPost({
            url: `tbwxsubscribemessage/update`,
            data: tbWxSubscribeMessage
        })
    }

    deleteTbWxSubscribeMessageById(id) {
        return this.httpUtil.myPost({
            url: `tbwxsubscribemessage/deleteById`,
            data: {id: id}
        })
    }
}
