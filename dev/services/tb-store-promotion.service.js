import {BaseService} from '../core/base/base.service'

export class TbStorePromotionService extends BaseService {

    findTableTbStorePromotionList(tbStorePromotion) {
        return this.httpUtil.myPost({
            url: `tbstorepromotion/findTableTbStorePromotionList?pageNum=1&pageSize=9999`,
            data: tbStorePromotion
        })
    }

    findTbStorePromotionList(tbStorePromotion) {
        return this.httpUtil.myPost({
            url: `tbstorepromotion/findList`,
            data: tbStorePromotion
        })
    }

    findTbOrderGoodsPromotionItem(tbStorePromotion) {
        return this.httpUtil.myPost({
            url: `tbstorepromotion/findTbOrderGoodsPromotionItem`,
            data: tbStorePromotion
        })
    }

    findTbOrderPromotion(tbOrder) {
        return this.httpUtil.myPostJson({
            url: `tbstorepromotion/findTbOrderPromotion`,
            data: tbOrder
        })
    }

    findTbStorePromotionById(id) {
        return this.httpUtil.myPost({
            url: `tbstorepromotion/findById`,
            data: {id: id}
        })
    }

    findTbStorePromotionByIdCascade(id) {
        return this.httpUtil.myPost({
            url: `tbstorepromotion/findTbStorePromotionByIdCascade`,
            data: {id: id}
        })
    }

    saveTbStorePromotion(tbStorePromotion) {
        return this.httpUtil.myPost({
            url: `tbstorepromotion/save`,
            data: tbStorePromotion,
            isOptimizeData: true
        })
    }

    updateTbStorePromotion(tbStorePromotion) {
        return this.httpUtil.myPost({
            url: `tbstorepromotion/update`,
            data: tbStorePromotion,
            isOptimizeData: true
        })
    }

    batchDeleteTbStorePromotionByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbstorepromotion/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }

    deleteTbStorePromotion(tbStorePromotion) {
        return this.httpUtil.myPost({
            url: `tbstorepromotion/deleteByIdCascade`,
            data: {id: tbStorePromotion.id}
        })
    }

    canEnable(tbStorePromotion) {
        return this.httpUtil.myPost({
            url: `tbstorepromotion/canEnable`,
            data: tbStorePromotion
        })
    }
}
