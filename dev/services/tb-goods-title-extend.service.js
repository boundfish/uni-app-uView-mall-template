import {BaseService} from '../core/base/base.service'

export class TbGoodsTitleExtendService extends BaseService {

    findTableTbGoodsListCascade(params) {
        return this.httpUtil.myPost({
            url: `tbgoodstitleextend/findTableTbGoodsListCascade?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTbGoodsTitleExtendList(tbGoodsTitleExtend) {
        return this.httpUtil.myPost({
            url: `tbgoodstitleextend/findList`,
            data: tbGoodsTitleExtend
        })
    }

    findTransferTbGoodsTitleExtendList(tbGoodsTitleExtend) {
        return this.httpUtil.myPost({
            url: `tbgoodstitleextend/findTransferTbGoodsTitleExtendList`,
            data: tbGoodsTitleExtend
        })
    }

    saveTbGoodsTitleExtend(tbGoodsTitleExtend) {
        return this.httpUtil.myPost({
            url: `tbgoodstitleextend/save`,
            data: tbGoodsTitleExtend
        })
    }

    batchSaveAssignedTbGoodsTitleExtends(tbGoodsTitleExtend) {
        return this.httpUtil.myPost({
            url: `tbgoodstitleextend/batchSaveAssignedTbGoodsTitleExtends`,
            data: tbGoodsTitleExtend
        })
    }

    updateTbGoodsTitleExtend(tbGoodsTitleExtend) {
        return this.httpUtil.myPost({
            url: `tbgoodstitleextend/update`,
            data: tbGoodsTitleExtend
        })
    }

    batchDeleteTbGoodsTitleExtendByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbgoodstitleextend/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }

    batchDelAssignedTbGoodsTitleExtends(goodsIds) {
        return this.httpUtil.myPost({
            url: `tbgoodstitleextend/batchDelAssignedTbGoodsTitleExtends`,
            data: {goodsIds: goodsIds}
        })
    }
}
