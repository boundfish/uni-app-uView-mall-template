import {BaseService} from '../core/base/base.service'

export class TbEmailPayCodeOrderService extends BaseService {

    sendPayCode(tbEmailPayCodeOrder) {
        return this.httpUtil.myPost({
            url: `tbemailpaycodeorder/sendPayCode`,
            data: tbEmailPayCodeOrder
        })
    }

    findTbEmailPayCodeOrderList(tbEmailPayCodeOrder) {
        return this.httpUtil.myPost({
            url: `tbemailpaycodeorder/findList`,
            data: tbEmailPayCodeOrder
        })
    }

    saveTbEmailPayCodeOrder(tbEmailPayCodeOrder) {
        return this.httpUtil.myPost({
            url: `tbemailpaycodeorder/save`,
            data: tbEmailPayCodeOrder
        })
    }

    updateTbEmailPayCodeOrder(tbEmailPayCodeOrder) {
        return this.httpUtil.myPost({
            url: `tbemailpaycodeorder/update`,
            data: tbEmailPayCodeOrder
        })
    }

    batchDeleteTbEmailPayCodeOrderByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbemailpaycodeorder/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }
}
