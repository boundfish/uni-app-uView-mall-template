import {BaseService} from '../core/base/base.service'

export class TbStoreCashDepositWithdrawalRecordService extends BaseService {

    findTbStoreCashDepositWithdrawalRecordList(tbStoreCashDepositWithdrawalRecord) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositwithdrawalrecord/findList`,
            data: tbStoreCashDepositWithdrawalRecord
        })
    }

    findTableList(params) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositwithdrawalrecord/findTableList?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTableTbStoreCashDepositWithdrawalRecordList(params) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositwithdrawalrecord/findTableTbStoreCashDepositWithdrawalRecordList?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    saveTbStoreCashDepositWithdrawalRecord(tbStoreCashDepositWithdrawalRecord) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositwithdrawalrecord/save`,
            data: tbStoreCashDepositWithdrawalRecord
        })
    }

    updateTbStoreCashDepositWithdrawalRecord(tbStoreCashDepositWithdrawalRecord) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositwithdrawalrecord/update`,
            data: tbStoreCashDepositWithdrawalRecord
        })
    }

    findTbStoreCashDepositWithdrawalRecordById(id) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositwithdrawalrecord/findById`,
            data: {id: id}
        })
    }

    findOne(tbStoreCashDepositWithdrawalRecord) {
        return this.httpUtil.myPost({
            url: `tbstorecashdepositwithdrawalrecord/findOne`,
            data: tbStoreCashDepositWithdrawalRecord
        })
    }

    withdrawalToAccount() {
        const _self = this;
        return this.httpUtil.myPost({
            url: `tbstorecashdepositwithdrawalrecord/withdrawalToAccount`,
        })
    }
}
