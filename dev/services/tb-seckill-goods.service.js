import {BaseService} from '../core/base/base.service'

export class TbSeckillGoodsService extends BaseService {

    findTableList(params) {
        return this.httpUtil.myPost({
            url: `tbseckillgoods/findTableList?${params.pagination.appendToUrl()}`,
            data: params.data,
            isOptimizeData: params.isOptimizeData
        })
    }

    findTableListCascade(params) {
        return this.httpUtil.myPost({
            url: `tbseckillgoods/findTableListCascade?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTableListByConsumer(params) {
        return this.httpUtil.myPost({
            url: `tbseckillgoods/findTableListByConsumer?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTableListByStore(params) {
        return this.httpUtil.myPost({
            url: `tbseckillgoods/findTableListByStore?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTbSeckillGoodsCount(tbSeckillGoods) {
        return this.httpUtil.myPost({
            url: `tbseckillgoods/findCount`,
            data: tbSeckillGoods
        })
    }

    seckillGoods(id) {
        return this.httpUtil.myPost({
            url: `tbseckillgoods/seckillGoods`,
            data: {id: id}
        })
    }

    findSeckillResult(id) {
        return this.httpUtil.myPost({
            url: `tbseckillgoods/findSeckillResult`,
            data: {id: id}
        })
    }

    saveTbSeckillGoods(tbSeckillGoods) {
        return this.httpUtil.myPost({
            url: `tbseckillgoods/save`,
            data: tbSeckillGoods
        })
    }

    updateTbSeckillGoods(tbSeckillGoods) {
        return this.httpUtil.myPost({
            url: `tbseckillgoods/update`,
            data: tbSeckillGoods
        })
    }

    batchDeleteTbSeckillGoodsByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbseckillgoods/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }
}
