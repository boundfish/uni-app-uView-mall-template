import {BaseService} from '../core/base/base.service'

export class TbOrderGoodsAfterSaleService extends BaseService {

    findTableListCascade(params) {
        return this.httpUtil.myPost({
            url: `tbordergoodsaftersale/findTableListCascade?${params.pagination.appendToUrl()}`,
            data: params.data,
        })
    }

    findTbOrderGoodsAfterSaleById(id) {
        return this.httpUtil.myPost({
            url: `tbordergoodsaftersale/findById`,
            data: {id: id}
        })
    }

    saveTbOrderGoodsAfterSale(tbOrderGoodsAfterSale) {
        return this.httpUtil.myPost({
            url: `tbordergoodsaftersale/saveTbOrderGoodsAfterSale`,
            data: tbOrderGoodsAfterSale
        })
    }

    updateTbOrderGoodsAfterSale(tbOrderGoodsAfterSale) {
        return this.httpUtil.myPost({
            url: `tbordergoodsaftersale/updateTbOrderGoodsAfterSale`,
            data: tbOrderGoodsAfterSale
        })
    }

    batchDeleteTbOrderGoodsAfterSaleByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbordergoodsaftersale/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }
}
