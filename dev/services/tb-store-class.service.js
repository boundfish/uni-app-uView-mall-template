import {BaseService} from '../core/base/base.service'

export class TbStoreClassService extends BaseService {

    findTableList(params) {
        return this.httpUtil.myPost({
            url: `tbstoreclass/findTableList?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTbStoreClassList(tbStoreClass) {
        return this.httpUtil.myPost({
            url: `tbstoreclass/findTbStoreClassList`,
            data: tbStoreClass
        })
    }

    findList(tbStoreClass) {
        return this.httpUtil.myPost({
            url: `tbstoreclass/findList`,
            data: tbStoreClass
        })
    }

    findMenu(tbStoreClass) {
        return this.httpUtil.myPost({
            url: `tbstoreclass/findMenu`,
            data: tbStoreClass
        })
    }

    findTbStoreClassTree(tbStoreClass) {
        return this.httpUtil.myPost({
            url: `tbstoreclass/findTree`,
            data: tbStoreClass
        })
    }

    saveTbStoreClass(tbStoreClass) {
        return this.httpUtil.myPost({
            url: `tbstoreclass/save`,
            data: tbStoreClass
        })
    }

    updateTbStoreClass(tbStoreClass) {
        return this.httpUtil.myPost({
            url: `tbstoreclass/update`,
            data: tbStoreClass
        })
    }

    deleteTbStoreClass(tbStoreClass) {
        return this.httpUtil.myPost({
            url: `tbstoreclass/deleteByIdCascade`,
            data: {id: tbStoreClass.id}
        })
    }
}
