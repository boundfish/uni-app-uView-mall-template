import {BaseService} from '../core/base/base.service'
import globalVariable from "../core/global-variable";

export class WechatService extends BaseService {

    /**
     * 通过微信小程序code获得unionid
     * {data: "unionid"}
     */
    findUnionidByWeixinMiniAppCode() {
        const _self = this;
        return new Promise(function (resolve, reject) {
            uni.login({
                success: (loginResult) => { // {errMsg: "login:ok", code: "0434TeKg2bXv6H0s6YMg2017Kg24TeK-"}
                    console.log("uni.login-----------------------------------------", loginResult);
                    if (loginResult && loginResult.code) {
                        _self.httpUtil.myPost({
                            url: 'wechat/findUnionidByWeixinMiniAppCode',
                            data: {
                                code: loginResult.code
                            }
                        }).then(result => {
                            resolve(result);
                        });
                    }
                }
            })
        });
    }

    /**
     * 通过微信APP应用code获得unionid
     * {data: "unionid"}
     */
    findUnionidByWeixinAppCode() {
        const _self = this;
        return new Promise(function (resolve, reject) {
            uni.login({
                provider: 'weixin',
                success: (loginResult) => { // {errMsg: "login:ok", code: "0434TeKg2bXv6H0s6YMg2017Kg24TeK-"}
                    console.log("uni.login-----------------------------------------", loginResult);
                    /*
                    loginResult =
                    {
                        "authResult": {
                            "access_token": "39_4qY3FkHNgBi3I4wcSW8oa3J0qgW2lwDXyDXWwxR1wzE72P-hwRuwBdTLLC5J317xtXf8higrxdlK3FDf4geRsayokLxKq-yA907d1xZCbBA",
                            "expires_in": 7200,
                            "refresh_token": "39_2hgRgy87wnjhQOzZ5DPUBnac7Te17i1mFBqyKBpwFvq2pzVhnjwIBThwLXHcLVoaVnA6PzMFZJTGm6ee4YxFpqhhjnC7OeqIHp1OD1O8jlk",
                            "openid": "oRrdQt3fvaYOXfeXLPOPdUvUYy1Y",
                            "scope": "snsapi_userinfo",
                            "unionid": "oU5Yyt8rwqDaowkhJyuzmYY-UUeo"  // todo 有时获取不到unionid
                        },
                        "errMsg": "login:ok"
                    }
                     */
                    if (loginResult && loginResult.authResult && loginResult.authResult.openid && loginResult.authResult.unionid) {
                        _self.httpUtil.myPost({
                            url: 'wechat/saveOpenidAndUnionidByWeixinApp',
                            data: {
                                openid: loginResult.authResult.openid,
                                unionid: loginResult.authResult.unionid,
                            }
                        }).then(result => {
                            resolve(result);
                        });
                    }
                }
            })
        });
    }


    /**
     * 根据code获得Unionid
     */
    findUnionidByCode() {
        if (globalVariable.runPlatform === "0") {
            return this.findUnionidByWeixinMiniAppCode();
        } else if (globalVariable.runPlatform === "1") {
            return this.findUnionidByWeixinAppCode();
        }
    }
}
