import {BaseService} from "../core/base/base.service";

export class TbFaqService extends BaseService {

    findTbFaqList(tbFaq) {
        return this.httpUtil.myPost({
            url: `tbfaq/findTbFaqList`,
            data: tbFaq,
            orderBy: 'display_order asc'
        });
    }

    findTbFaqDetailById(tbFaq) {
        return this.httpUtil.myPost({
            url: `tbfaq/findTbFaqDetailById`,
            data: tbFaq,
        });
    }

    saveTbFaq(tbFaq) {
        return this.httpUtil.myPost({
            url: `tbfaq/save`,
            data: tbFaq
        });
    }

    updateTbFaq(tbFaq) {
        return this.httpUtil.myPost({
            url: `tbfaq/update`,
            data: tbFaq
        });
    }

    deleteTbFaqByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbfaq/deleteByIds`,
            data: {ids: ids}
        });
    }
}
