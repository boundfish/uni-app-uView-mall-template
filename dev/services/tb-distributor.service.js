import {BaseService} from "../core/base/base.service";

export class TbDistributorService extends BaseService {

    findTableListCascade(params) {
        return this.httpUtil.myPost({
            url: `tbdistributor/findTableListCascade?${params.pagination.appendToUrl()}`,
            data: params.data
        });
    }

    findTbDistributorList(tbDistributor) {
        return this.httpUtil.myPost({
            url: `tbdistributor/findList`,
            data: tbDistributor,
            orderBy: 'display_order asc'
        });
    }

    saveTbDistributor(tbDistributor) {
        return this.httpUtil.myPost({
            url: `tbdistributor/save`,
            data: tbDistributor
        });
    }

    setInviter(data) {
        return this.httpUtil.myPost({
            url: `tbdistributor/setInviter`,
            data: data
        });
    }

    updateTbDistributor(tbDistributor) {
        return this.httpUtil.myPost({
            url: `tbdistributor/update`,
            data: tbDistributor
        });
    }

    deleteTbDistributorByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbdistributor/deleteByIds`,
            data: {ids: ids}
        });
    }
}
