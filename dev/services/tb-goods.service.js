import {BaseService} from '../core/base/base.service'

export class TbGoodsService extends BaseService {

    findList(tbGoods) {
        return this.httpUtil.myPost({
            url: `tbgoods/findList`,
            data: tbGoods
        })
    }

    findTableList(params) {
        return this.httpUtil.myPost({
            url: `tbgoods/findTableList?${params.pagination.appendToUrl()}`,
            data: params.data,
            isOptimizeData: params.isOptimizeData
        })
    }

    findTableListByStore(params) {
        return this.httpUtil.myPost({
            url: `tbgoods/findTableListByStore?${params.pagination.appendToUrl()}`,
            data: params.data,
            isOptimizeData: params.isOptimizeData
        })
    }

    findTableListByConsumer(params) {
        return this.httpUtil.myPost({
            url: `tbgoods/findTableListByConsumer?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTableListByLucene(params) {
        return this.httpUtil.myPost({
            url: `tbgoods/findTableListByLucene?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTableListCascade(params) {
        return this.httpUtil.myPost({
            url: `tbgoods/findTableListCascade?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findGoodsDetail(id) {
        return this.httpUtil.myPost({
            url: `tbgoods/findGoodsDetail`,
            data: {id: id}
        })
    }

    findGoodsById(id) {
        return this.httpUtil.myPost({
            url: `tbgoods/findById`,
            data: {id: id}
        })
    }

    isValidGoods(tbGoods) {
        return this.httpUtil.myPost({
            url: `tbgoods/isValidGoods`,
            data: tbGoods
        })
    }

    saveTbGoods(tbGoods) {
        return this.httpUtil.myPost({
            url: `tbgoods/save`,
            data: tbGoods
        })
    }

    updateTbGoods(tbGoods) {
        return this.httpUtil.myPost({
            url: `tbgoods/update`,
            data: tbGoods
        })
    }

    batchDeleteTbGoodsByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbgoods/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }
}
