import {BaseService} from '../core/base/base.service'

export class SysUserService extends BaseService {

    findTableList(params) {
        return this.httpUtil.myPost({
            url: `sysuser/findTableList?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findById(id) {
        return this.httpUtil.myPost({
            url: `sysuser/findById`,
            data: {id: id}
        })
    }

    saveSysUser(sysUser) {
        return this.httpUtil.myPost({
            url: `sysuser/save`,
            data: sysUser
        })
    }

    register(sysUser) {
        return this.httpUtil.myPost({
            url: `sysuser/register`,
            data: sysUser
        })
    }

    validRawPassword(sysUser) {
        const _self = this;
        return _self.httpUtil.myPost({
            url: `sysuser/validRawPassword`,
            data: sysUser
        })
    }

    updatePassword(sysUser) {
        return this.httpUtil.myPost({
            url: `sysuser/updatePassword`,
            data: sysUser
        })
    }

    changePhone(sysUser) {
        return this.httpUtil.myPost({
            url: `sysuser/changePhone`,
            data: sysUser
        })
    }

    updateSysUser(sysUser) {
        return this.httpUtil.myPost({
            url: `sysuser/update`,
            data: sysUser
        })
    }

    updateSessionData(sysUser) {
        return this.httpUtil.myPostJson({
            url: `sysuser/updateSessionData`,
            data: sysUser
        })
    }

    markTbCommunityUserTypeById(id) {
        return this.httpUtil.myPost({
            url: `sysuser/markTbCommunityUserTypeById`,
            data: {id: id}
        })
    }

    batchDeleteSysUserList(ids) {
        return this.httpUtil.myPost({
            url: `sysuser/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }
}
