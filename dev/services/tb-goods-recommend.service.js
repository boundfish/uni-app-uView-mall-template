import {BaseService} from '../core/base/base.service'

export class TbGoodsRecommendService extends BaseService {

    findTbGoodsRecommendList(tbGoodsRecommend) {
        return this.httpUtil.myPost({
            url: `tbgoodsrecommend/findList`,
            data: tbGoodsRecommend,
        })
    }

    findTbGoodsRecommendListCascade(tbGoodsRecommend) {
        return this.httpUtil.myPost({
            url: `tbgoodsrecommend/findTbGoodsRecommendListCascade`,
            data: tbGoodsRecommend,
        })
    }

    findTbGoodsRecommendById(id) {
        return this.httpUtil.myPost({
            url: `tbgoodsrecommend/findById`,
            data: {id: id},
        })
    }

    saveTbGoodsRecommend(tbGoodsRecommend) {
        return this.httpUtil.myPost({
            url: `tbgoodsrecommend/save`,
            data: tbGoodsRecommend
        })
    }

    updateTbGoodsRecommend(tbGoodsRecommend) {
        return this.httpUtil.myPost({
            url: `tbgoodsrecommend/update`,
            data: tbGoodsRecommend
        })
    }

    deleteTbGoodsRecommendById(id) {
        return this.httpUtil.myPost({
            url: `tbgoodsrecommend/deleteById`,
            data: {id: id}
        })
    }
}
