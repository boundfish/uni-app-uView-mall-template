import {BaseService} from '../core/base/base.service'

export class TbShoppingCartService extends BaseService {

    findTbShoppingCartCount(tbShoppingCart) {
        return this.httpUtil.myPost({
            url: `tbshoppingcart/findCount`,
            data: tbShoppingCart
        })
    }

    findTbShoppingCartList(tbShoppingCart) {
        return this.httpUtil.myPost({
            url: `tbshoppingcart/findListCascade`,
            data: tbShoppingCart
        })
    }

    saveTbShoppingCart(tbShoppingCart) {
        return this.httpUtil.myPost({
            url: `tbshoppingcart/saveTbShoppingCart`,
            data: tbShoppingCart
        })
    }

    updateTbShoppingCart(tbShoppingCart) {
        return this.httpUtil.myPost({
            url: `tbshoppingcart/update`,
            data: tbShoppingCart
        })
    }

    batchDeleteTbShoppingCartByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbshoppingcart/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }
}
