import {BaseService} from '../core/base/base.service'

export class TbStoreOrderSequenceNumberService extends BaseService {

    findTableListCascade(params) {
        return this.httpUtil.myPost({
            url: `tbstoreordersequencenumber/findTableListCascade?${params.pagination.appendToUrl()}`,
            data: params.data,
        })
    }

    findTbStoreOrderSequenceNumberList(tbStoreOrderSequenceNumber) {
        return this.httpUtil.myPost({
            url: `tbstoreordersequencenumber/findList`,
            data: tbStoreOrderSequenceNumber,
        })
    }

    saveTbStoreOrderSequenceNumber(tbStoreOrderSequenceNumber) {
        return this.httpUtil.myPost({
            url: `tbstoreordersequencenumber/save`,
            data: tbStoreOrderSequenceNumber
        })
    }

    updateTbStoreOrderSequenceNumber(tbStoreOrderSequenceNumber) {
        return this.httpUtil.myPost({
            url: `tbstoreordersequencenumber/update`,
            data: tbStoreOrderSequenceNumber
        })
    }

    deleteTbStoreOrderSequenceNumber(tbStoreOrderSequenceNumber) {
        return this.httpUtil.myPost({
            url: `tbstoreordersequencenumber/deleteById`,
            data: {id: tbStoreOrderSequenceNumber.id}
        })
    }
}
