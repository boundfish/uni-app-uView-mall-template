import {BaseService} from '../core/base/base.service'

export class TbStoreSenderService extends BaseService {

    findTbStoreSenderList(tbStoreSender) {
        return this.httpUtil.myPost({
            url: `tbstoresender/findList`,
            data: tbStoreSender,
        })
    }

    findTbStoreSenderById(id) {
        return this.httpUtil.myPost({
            url: `tbstoresender/findById`,
            data: {id: id},
        })
    }

    saveTbStoreSender(tbStoreSender) {
        return this.httpUtil.myPost({
            url: `tbstoresender/save`,
            data: tbStoreSender
        })
    }

    updateTbStoreSender(tbStoreSender) {
        return this.httpUtil.myPost({
            url: `tbstoresender/update`,
            data: tbStoreSender
        })
    }

    deleteTbStoreSenderById(id) {
        return this.httpUtil.myPost({
            url: `tbstoresender/deleteById`,
            data: {id: id}
        })
    }
}
