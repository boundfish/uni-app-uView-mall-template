import {BaseService} from '../core/base/base.service'
import {WechatService} from "./wechat.service";

export class SysUserThirdLoginService extends BaseService {

    findSysUserThirdLoginList(sysUserThirdLogin) {
        return this.httpUtil.myPost({
            url: `sysuserthirdlogin/findList`,
            data: sysUserThirdLogin,
            orderBy: 'display_order asc'
        })
    }

    /**
     * 微信登录授权-unionid
     */
    weixinLoginAuthorizationByUnionid(params) {
        const _self = this;
        return new Promise(function (resolve, reject) {
            return _self.httpUtil.myPost({
                url: 'sysuserthirdlogin/weixinLoginAuthorizationByUnionid',
                data: params
            }).then(result => {
                resolve(result);
            });
        });
    }

    /**
     * 微信登录-unionid版
     */
    weixinLoginByUnionid(name) {
        const _self = this;
        return new Promise(function (resolve, reject) {
            new WechatService().findUnionidByCode().then(result1 => {
                if (result1 && result1.data) {
                    _self.httpUtil.myPost({
                        url: 'sysuserthirdlogin/weixinLoginByUnionid',
                        data: {
                            unionid: result1.data,
                        }
                    }).then(result2 => {
                        resolve(result2);
                    });
                }
            });
        });
    }

    deleteSysUserThirdLoginByIds(ids) {
        return this.httpUtil.myPost({
            url: `sysuserthirdlogin/deleteByIds`,
            data: {ids: ids}
        })
    }
}
