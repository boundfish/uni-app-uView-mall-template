import {BaseService} from '../core/base/base.service'

export class TbStoreAdTemplateService extends BaseService {

    findListCascade(tbStoreAdTemplate) {
        return this.httpUtil.myPost({
            url: `tbstoreadtemplate/findListCascade`,
            data: tbStoreAdTemplate
        })
    }

    findTbStoreAdTemplateList(tbStoreAdTemplate) {
        return this.httpUtil.myPost({
            url: `tbstoreadtemplate/findList`,
            data: tbStoreAdTemplate
        })
    }

    saveTbStoreAdTemplate(tbStoreAdTemplate) {
        return this.httpUtil.myPost({
            url: `tbstoreadtemplate/save`,
            data: tbStoreAdTemplate
        })
    }

    updateAssociatedStore(tbStoreAdTemplate) {
        return this.httpUtil.myPost({
            url: `tbstoreadtemplate/updateAssociatedStore`,
            data: tbStoreAdTemplate
        })
    }

    updateTbStoreAdTemplate(tbStoreAdTemplate) {
        return this.httpUtil.myPost({
            url: `tbstoreadtemplate/update`,
            data: tbStoreAdTemplate
        })
    }

    batchDeleteTbStoreAdTemplateByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbstoreadtemplate/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }
}
