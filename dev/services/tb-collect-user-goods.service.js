import {BaseService} from '../core/base/base.service'

export class TbCollectUserGoodsService extends BaseService {
  findTbCollectUserGoodsList (tbCollectUserGoods) {
    return this.httpUtil.myPost({
      url: `tbcollectusergoods/findList`,
      data: tbCollectUserGoods
    })
  }

  findTableListCascade (params) {
    return this.httpUtil.myPost({
      url: `tbcollectusergoods/findTableListCascade?${params.pagination.appendToUrl()}`,
      data: params.data
    })
  }

  saveTbCollectUserGoods (tbCollectUserGoods) {
    return this.httpUtil.myPost({
      url: `tbcollectusergoods/save`,
      data: tbCollectUserGoods
    })
  }

  updateTbCollectUserGoods (tbCollectUserGoods) {
    return this.httpUtil.myPost({
      url: `tbcollectusergoods/update`,
      data: tbCollectUserGoods
    })
  }

  deleteTbCollectUserGoods (tbCollectUserGoods) {
    return this.httpUtil.myPost({
      url: `tbcollectusergoods/deleteById`,
      data: tbCollectUserGoods
    })
  }

  batchDeleteTbCollectUserGoodsByIds (ids) {
    return this.httpUtil.myPost({
      url: `tbcollectusergoods/deleteByIds`,
      data: {
        ids: ids
      }
    })
  }

  findCollectNum (tbCollectUserGoods) {
    return this.httpUtil.myPost({
      url: `tbcollectusergoods/findCollectNum`,
      data: tbCollectUserGoods
    })
  }

  isCollectedGoods (tbCollectUserGoods) {
    return this.httpUtil.myGet({
      url: `tbcollectusergoods/isCollected`,
      data: tbCollectUserGoods
    })
  }
}
