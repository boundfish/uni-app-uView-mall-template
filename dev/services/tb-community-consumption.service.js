import {BaseService} from "../core/base/base.service";

export class TbCommunityConsumptionService extends BaseService {

    findTbCommunityConsumptionList(tbCommunityConsumption) {
        return this.httpUtil.myPost({
            url: `tbcommunityconsumption/findList`,
            data: tbCommunityConsumption,
        });
    }

    findTop3List(params) {
        return this.httpUtil.myPost({
            url: `tbcommunityconsumption/findTableTopList?pageNum=1&pageSize=3`,
            data: params.data,
        });
    }

    findTableTopList(params) {
        return this.httpUtil.myPost({
            url: `tbcommunityconsumption/findTableTopList?${params.pagination.appendToUrl()}`,
            data: params.data,
        });
    }

    findTableTbCommunityConsumptionList(params) {
        return this.httpUtil.myPost({
            url: `tbcommunityconsumption/findTableListCascade?${params.pagination.appendToUrl()}`,
            data: params.data,
        });
    }

    findTbCommunityConsumption(tbCommunityConsumption) {
        return this.httpUtil.myPost({
            url: `tbcommunityconsumption/findOne`,
            data: tbCommunityConsumption,
        });
    }

    findTbCommunityConsumptionById(id) {
        return this.httpUtil.myPost({
            url: `tbcommunityconsumption/findById`,
            data: {id: id},
        });
    }

    isConsumption(data) {
        return this.httpUtil.myPost({
            url: `tbcommunityconsumption/isConsumption`,
            data: data,
        });
    }

    saveTbCommunityConsumption(tbCommunityConsumption) {
        return this.httpUtil.myPost({
            url: `tbcommunityconsumption/save`,
            data: tbCommunityConsumption
        });
    }

    updateTbCommunityConsumption(tbCommunityConsumption) {
        return this.httpUtil.myPost({
            url: `tbcommunityconsumption/update`,
            data: tbCommunityConsumption
        });
    }

    deleteTbCommunityConsumptionById(id) {
        return this.httpUtil.myPost({
            url: `tbcommunityconsumption/deleteById`,
            data: {id: id}
        });
    }
}
