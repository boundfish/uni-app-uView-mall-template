import {BaseService} from '../core/base/base.service'

export class TbGoodsSpecificationColorService extends BaseService {

    findTableListCascade(tbGoodsSpecificationColor) {
        return this.httpUtil.myPost({
            url: `tbgoodsspecificationcolor/findTableListCascade?pageNum=1&pageSize=999999`,
            data: tbGoodsSpecificationColor
        })
    }

    findTbGoodsSpecificationColorList(tbGoodsSpecificationColor) {
        return this.httpUtil.myPost({
            url: `tbgoodsspecificationcolor/findList`,
            data: tbGoodsSpecificationColor
        })
    }

    findTbGoodsSpecificationColorCount(tbGoodsSpecificationColor) {
        return this.httpUtil.myPost({
            url: `tbgoodsspecificationcolor/findCount`,
            data: tbGoodsSpecificationColor
        })
    }

    saveTbGoodsSpecificationColor(tbGoodsSpecificationColor) {
        return this.httpUtil.myPostJson({
            url: `tbgoodsspecificationcolor/saveTbGoodsSpecificationColor`,
            data: tbGoodsSpecificationColor
        })
    }

    updateTbGoodsSpecificationColor(tbGoodsSpecificationColor) {
        return this.httpUtil.myPostJson({
            url: `tbgoodsspecificationcolor/updateTbGoodsSpecificationColor`,
            data: tbGoodsSpecificationColor
        })
    }

    batchDeleteTbGoodsSpecificationColorByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbgoodsspecificationcolor/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }
}
