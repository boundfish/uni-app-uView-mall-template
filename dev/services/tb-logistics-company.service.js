import {BaseService} from '../core/base/base.service'

export class TbLogisticsCompanyService extends BaseService {

    findTbLogisticsCompanyList(tbLogisticsCompany) {
        return this.httpUtil.myPost({
            url: `tblogisticscompany/findList`,
            data: tbLogisticsCompany
        })
    }

    findTableTbLogisticsCompanyList(params) {
        return this.httpUtil.myPost({
            url: `tblogisticscompany/findTableList?${params.pagination.appendToUrl(null, 20)}`,
            data: params.data
        })
    }

    saveTbLogisticsCompany(tbLogisticsCompany) {
        return this.httpUtil.myPost({
            url: `tblogisticscompany/save`,
            data: tbLogisticsCompany
        })
    }

    updateTbLogisticsCompany(tbLogisticsCompany) {
        return this.httpUtil.myPost({
            url: `tblogisticscompany/update`,
            data: tbLogisticsCompany
        })
    }

    batchDeleteTbLogisticsCompanyByIds(ids) {
        return this.httpUtil.myPost({
            url: `tblogisticscompany/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }
}
