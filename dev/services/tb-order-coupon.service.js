import {BaseService} from '../core/base/base.service'

export class TbOrderCouponService extends BaseService {

    findTableTbOrderCouponList(params) {
        return this.httpUtil.myPost({
            url: `tbordercoupon/findTableTbOrderCouponList?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findTbOrderCouponList(tbOrderCoupon) {
        return this.httpUtil.myPost({
            url: `tbordercoupon/findList`,
            data: tbOrderCoupon,
            orderBy: 'display_order asc'
        })
    }

    saveTbOrderCoupon(tbOrderCoupon) {
        return this.httpUtil.myPost({
            url: `tbordercoupon/saveTbOrderCoupon`,
            data: tbOrderCoupon
        })
    }

    updateTbOrderCoupon(tbOrderCoupon) {
        return this.httpUtil.myPost({
            url: `tbordercoupon/update`,
            data: tbOrderCoupon
        })
    }

    deleteTbOrderCouponByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbordercoupon/deleteByIds`,
            data: {ids: ids}
        })
    }
}
