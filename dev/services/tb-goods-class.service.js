import {BaseService} from '../core/base/base.service'

export class TbGoodsClassService extends BaseService {

    findTbGoodsClassList(tbGoodsClass) {
        return this.httpUtil.myPost({
            url: `tbgoodsclass/findList`,
            data: tbGoodsClass
        })
    }

    findTbGoodsClassById(id) {
        return this.httpUtil.myPost({
            url: `tbgoodsclass/findById`,
            data: {id: id}
        })
    }

    saveTbGoodsClass(tbGoodsClass) {
        return this.httpUtil.myPost({
            url: `tbgoodsclass/save`,
            data: tbGoodsClass
        })
    }

    updateTbGoodsClass(tbGoodsClass) {
        return this.httpUtil.myPost({
            url: `tbgoodsclass/update`,
            data: tbGoodsClass
        })
    }

    batchDeleteTbGoodsClassByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbgoodsclass/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }

    deleteTbGoodsClassByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbgoodsclass/deleteTbGoodsClassByIds`,
            data: {
                ids: ids
            }
        })
    }
}
