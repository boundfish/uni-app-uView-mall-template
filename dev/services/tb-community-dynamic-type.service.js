import {BaseService} from "../core/base/base.service";

export class TbCommunityDynamicTypeService extends BaseService {

    /**
     * 查找动态分类，并包含每个分类最近5条动态
     */
    findTbCommunityDynamicTypeListContainLatestDynamic(tbCommunityDynamicType) {
        return this.httpUtil.myPost({
            url: `tbcommunitydynamictype/findTbCommunityDynamicTypeListContainLatestDynamic`,
            data: tbCommunityDynamicType
        });
    }

    findTbCommunityDynamicTypeList(tbCommunityDynamicType) {
        return this.httpUtil.myPost({
            url: `tbcommunitydynamictype/findList`,
            data: tbCommunityDynamicType
        });
    }

    findTableList(params) {
        return this.httpUtil.myPost({
            url: `tbcommunitydynamictype/findTableList?${params.pagination.appendToUrl()}`,
            data: params.data,
        });
    }

    findTableTbCommunityDynamicTypeList(params) {
        return this.httpUtil.myPost({
            url: `tbcommunitydynamictype/findTableTbCommunityDynamicTypeList?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    saveTbCommunityDynamicType(tbCommunityDynamicType) {
        return this.httpUtil.myPost({
            url: `tbcommunitydynamictype/save`,
            data: tbCommunityDynamicType
        });
    }

    updateTbCommunityDynamicType(tbCommunityDynamicType) {
        return this.httpUtil.myPost({
            url: `tbcommunitydynamictype/update`,
            data: tbCommunityDynamicType
        });
    }

    batchDeleteTbCommunityDynamicTypeByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbcommunitydynamictype/deleteByIds`,
            data: {
                ids: ids
            }
        });
    }
}
