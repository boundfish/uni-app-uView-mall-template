import {BaseService} from '../core/base/base.service'

export class SysConfigService extends BaseService {

    findSysConfig(sysConfig) {
        return this.httpUtil.myPost({
            url: `sysconfig/findOne`,
            data: sysConfig
        })
    }

    saveSysConfig(sysConfig) {
        return this.httpUtil.myPost({
            url: `sysconfig/save`,
            data: sysConfig
        })
    }

    updateSysConfig(sysConfig) {
        return this.httpUtil.myPost({
            url: `sysconfig/update`,
            data: sysConfig
        })
    }

    batchDeleteSysConfigByIds(ids) {
        return this.httpUtil.myPost({
            url: `sysconfig/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }
}
