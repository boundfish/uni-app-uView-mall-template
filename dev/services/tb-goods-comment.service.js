import {BaseService} from '../core/base/base.service'

export class TbGoodsCommentService extends BaseService {

    findTree(tbGoodsComment) {
        return this.httpUtil.myPost({
            url: `tbgoodscomment/findTree`,
            data: tbGoodsComment
        })
    }

    findTbGoodsCommentList(tbGoodsComment) {
        return this.httpUtil.myPost({
            url: `tbgoodscomment/findList`,
            data: tbGoodsComment
        })
    }

    findTableTbGoodsCommentList(params) {
        return this.httpUtil.myPost({
            url: `tbgoodscomment/findTableListCascade?${params.pagination.appendToUrl()}`,
            data: params.data
        })
    }

    findReplyTableListCascade(params) {
        return this.httpUtil.myPost({
            url: `tbgoodscomment/findReplyTableListCascade?${params.pagination.appendToUrl()}`,
            data: params.data,
        })
    }

    findTbGoodsComment(tbGoodsComment) {
        return this.httpUtil.myPost({
            url: `tbgoodscomment/findTbGoodsComment`,
            data: tbGoodsComment
        })
    }

    saveTbGoodsComment(tbGoodsComment) {
        return this.httpUtil.myPost({
            url: `tbgoodscomment/save`,
            data: tbGoodsComment
        })
    }

    batchSaveTbGoodsComment(tbGoodsCommentList) {
        return this.httpUtil.myPostJson({
            url: `tbgoodscomment/batchSaveTbGoodsCommentList`,
            data: tbGoodsCommentList
        })
    }

    updateTbGoodsComment(tbGoodsComment) {
        return this.httpUtil.myPost({
            url: `tbgoodscomment/update`,
            data: tbGoodsComment
        })
    }

    batchDeleteTbGoodsCommentByIds(ids) {
        return this.httpUtil.myPost({
            url: `tbgoodscomment/deleteByIds`,
            data: {
                ids: ids
            }
        })
    }

    addOrDelTbGoodsCommentLike(params) {
        return this.httpUtil.myPost({
            url: `tbgoodscomment/addOrDelTbGoodsCommentLike`,
            data: params
        })
    }
}
