import {BaseService} from '../core/base/base.service'

export class TbCommunityHomeConfigService extends BaseService {

    findTbCommunityHomeConfigList(tbCommunityHomeConfig) {
        return this.httpUtil.myPost({
            url: `tbcommunityhomeconfig/findList`,
            data: tbCommunityHomeConfig,
        })
    }

    findTbCommunityHomeConfigByCommunityId(communityId) {
        return this.httpUtil.myPost({
            url: `tbcommunityhomeconfig/findTbCommunityHomeConfigByCommunityId`,
            data: {communityId: communityId},
        })
    }

    findTbCommunityHomeConfigById(id) {
        return this.httpUtil.myPost({
            url: `tbcommunityhomeconfig/findById`,
            data: {id: id},
        })
    }

    saveTbCommunityHomeConfig(tbCommunityHomeConfig) {
        return this.httpUtil.myPost({
            url: `tbcommunityhomeconfig/save`,
            data: tbCommunityHomeConfig
        })
    }

    updateTbCommunityHomeConfig(tbCommunityHomeConfig) {
        return this.httpUtil.myPost({
            url: `tbcommunityhomeconfig/update`,
            data: tbCommunityHomeConfig
        })
    }

    deleteTbCommunityHomeConfigById(id) {
        return this.httpUtil.myPost({
            url: `tbcommunityhomeconfig/deleteById`,
            data: {id: id}
        })
    }
}
