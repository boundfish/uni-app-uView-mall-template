import {HttpUtil} from '../../core/utils/http.util'
import {LocalStorageUtil} from '../utils/local-storage.util'
import globalVariable from "../global-variable";

export class BaseService {

    httpUtil = HttpUtil

    globalVariable = globalVariable

    localStorageUtil = LocalStorageUtil
}
