const globalVariable = {

    key: '',  // 数据签名key
    localStoragePrefix: 'localStorage:demo-uview:app:',  // todo 不同平台这里需要修改
    // localStoragePrefix: 'localStorage:wlsh-uview:app:',  // todo 不同平台这里需要修改
    timestamp: new Date().getTime(),

    baseUrl: 'http://xxxxxxxxxxxxxxxxxxxxxxxx:33000/demo-mobile/',  // 服务端地址

    systemName: 'uview商城模板',

    runPlatform: "1",  // 0-微信小程序版，1-APP版，2-h5版 todo 不同平台这里需要修改

    userInfo: {},

    token: null,

    coordinate: "",  // 手机坐标 -- > 113.41189,22.94181

    orderSequenceNumber: null,  // 餐饮号

    tempData: {},  // 存放临时数据，例如跳转页面时获取前一个页面保存的数据。
};

export default globalVariable
