export class PaginationModel {
    /**
     * 页码，从1开始
     */
    pageNum = 1;
    /**
     * 页面大小
     */
    pageSize = 10;

    size = 0;

    orderBy = null;

    /**
     * 起始行
     */
    startRow = 1;
    /**
     *
     * 末行
     */
    endRow = 0;
    /**
     * 总数
     */
    total = 0;
    /**
     * 总页数
     */
    pages = 0;

    data = [];

    firstPage = 1;

    prePage = 0;

    nextPage = 0;

    lastPage = 1;

    isFirstPage = true;

    /**
     * 是否是最后一页
     */
    isLastPage = false;

    hasPreviousPage = false

    /**
     * 是否有下一页
     */
    hasNextPage = false;

    navigatePages = 8;

    navigatepageNums = [1];

    isLast = false;

    // <!--通过status设置组件的状态，加载前值为loadmore，加载中为loading，没有数据为nomore-->
    status = 'nomore';

    refreshPage(data) {
        for (let attr in data) {
            this[attr] = data[attr]
        }
        if (this.hasNextPage) {
            this.pageNum = this.pageNum + 1
            this.status = 'loadmore'
        } else {
            this.status = 'nomore'
        }
        return data
    }

    /**
     * 分页
     * pagination[page]
     * pagination[perpage]
     * 排序
     * sort[field]
     * sort[sort]
     */
    appendToUrl(pageNum, pageSize) {
        if (pageNum) {
            this.pageNum = pageNum;
        }
        if (pageSize) {
            this.pageSize = pageSize;
        }
        return `pageNum=${this.pageNum}&pageSize=${this.pageSize}`
    }
}
