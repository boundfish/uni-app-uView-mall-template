import {HttpUtil} from "./http.util";
import globalVariable from "../global-variable";
import {WechatService} from "../../services/wechat.service";
import {LocalStorageUtil} from "./local-storage.util";

/**
 * 初始化支付环境
 */
export function initPayEnvironment() {
    const _self = this;
    console.log("globalVariable---------------------", globalVariable);
    return new Promise(function (resolve, reject) {
        // 微信小程序版-微信支付
        if (globalVariable.runPlatform === "0") {
            console.log("准备获得微信小程序code");

            // 如果没有openid则无法支付，需要先授权获取unionid
            if (!globalVariable.userInfo.unionid) {
                new WechatService().findUnionidByWeixinMiniAppCode().then(result => {
                    globalVariable.userInfo.unionid = result.data;
                    LocalStorageUtil.setItem("userInfo", globalVariable.userInfo);
                    resolve('支付环境初始化成功!');
                })
            } else {
                resolve('支付环境初始化成功!');
            }
        } else if (globalVariable.runPlatform === "1") {
            console.log("准备获得微信应用code");

            // 如果没有openid则无法支付，需要先授权获取unionid
            if (!globalVariable.userInfo.unionid) {
                new WechatService().findUnionidByWeixinAppCode().then(result => {
                    globalVariable.userInfo.unionid = result.data;
                    LocalStorageUtil.setItem("userInfo", globalVariable.userInfo);
                    resolve('支付环境初始化成功!');
                })
            } else {
                resolve('支付环境初始化成功!');
            }
        }
    });
}

/**
 * 支付订单
 */
export function pay_order(params) {
    if (globalVariable.runPlatform === "0") {
        wxMiniAppPay(params);
    } else if (globalVariable.runPlatform === "1") {
        appPay(params);
    }
}

/**
 * 充值店铺保证金
 */
export function pay_tbStoreCashDeposit(params) {
    if (globalVariable.runPlatform === "0") {
        wxMiniAppPay_tbStoreCashDeposit(params);
    } else if (globalVariable.runPlatform === "1") {
        appPay_tbStoreCashDeposit(params)
    }
}

/**
 * 购买邮箱支付码
 */
export function pay_tbEmailPayCodeOrder(params) {
    if (globalVariable.runPlatform === "0") {
        wxMiniAppPay_tbEmailPayCodeOrder(params);
    } else if (globalVariable.runPlatform === "1") {
        appPay_tbEmailPayCodeOrder(params)
    }
}

// todo ------------------------------------------------微信小程序支付------------------------------------------------
// todo ------------------------------------------------微信小程序支付------------------------------------------------
// todo ------------------------------------------------微信小程序支付------------------------------------------------
// todo ------------------------------------------------微信小程序支付------------------------------------------------
// todo ------------------------------------------------微信小程序支付------------------------------------------------
/**
 * 微信支付-微信小程序版
 * @param params
 */
export function wxMiniAppPay(params) {
    HttpUtil.myPost({ // 封装微信请求方法
        url: 'wxPay/miniAppPay',
        data: {
            unionid: globalVariable.userInfo.unionid,
            orderId: params.orderId, // 根据商户自己的秘钥进行支付，后台没有查询到则使用自己默认的。
        }
    }).then(result => {
        // 支付密钥生成成功
        if (result.code === 0) {

            // 支付密钥生成成功，调起支付功能
            uni.requestPayment({
                timeStamp: result.data.timeStamp,
                nonceStr: result.data.nonceStr,
                package: result.data.package,
                signType: result.data.signType,
                paySign: result.data.paySign,
                success: (res) => {

                    // 支付成功！ 请求后台校验是否支付成功
                    HttpUtil.myPost({
                        url: 'wxPay/miniAppPayOrderQuery',
                        data: {
                            orderId: params.orderId
                        }
                    }).then(queryResult => {
                        if (queryResult.code === 0) { // 校验成功/支付成功
                            uni.showToast({
                                title: '支付成功！',
                                icon: 'success',
                                duration: 1500,
                                mask: true,
                                complete: () => {
                                    if (params.callback) {
                                        setTimeout(() => {
                                            params.callback(queryResult.data)
                                        }, 1500)
                                    }
                                }
                            })
                        } else {
                            uni.showToast({
                                title: '支付失败！',
                                icon: 'warn',
                                duration: 1500,
                                mask: true,
                                complete: () => {
                                    if (params.callback) {
                                        setTimeout(() => {
                                            params.callback(null)
                                        }, 1500)
                                    }
                                }
                            })
                        }

                    })
                },
                fail: function (res) {
                    uni.showToast({
                        title: '支付失败！',
                        icon: 'warn',
                        mask: true,
                        duration: 1500,
                        complete: () => {
                            if (params.callback) {
                                setTimeout(() => {
                                    params.callback(null)
                                }, 1500)
                            }
                        }
                    })
                },
                complete: function (res) {
                    console.log(res)
                }
            })

        } else {  // 支付密钥生成失败
            uni.showToast({
                title: '支付失败！',
                icon: 'warn',
                duration: 1500,
                mask: true,
            })
        }
    })
}

/**
 * 微信支付-微信小程序版
 * 充值店铺保证金
 */
export function wxMiniAppPay_tbStoreCashDeposit(params) {
    const _self = this;
    HttpUtil.myPost({ // 封装微信请求方法
        url: 'wxPay/miniAppPay_tbStoreCashDeposit',
        data: {
            unionid: globalVariable.userInfo.unionid,
            payMoney: params.payMoney
        }
    }).then(result => {

        // 支付密钥生成成功
        if (result.code === 0) {

            // 支付密钥生成成功，调起支付功能
            uni.requestPayment({
                timeStamp: result.data.timeStamp,
                nonceStr: result.data.nonceStr,
                package: result.data.package,
                signType: result.data.signType,
                paySign: result.data.paySign,
                success: (res) => {
                    // 支付成功！ 请求后台校验是否支付成功
                    HttpUtil.myPost({
                        url: 'wxPay/miniAppPay_tbStoreCashDepositQuery',
                        data: {
                            code: result.data.code, // 订单编号
                        }
                    }).then(queryResult => {
                        if (queryResult.code === 0) { // 校验成功/支付成功
                            uni.showToast({
                                title: '支付成功！',
                                icon: 'success',
                                mask: true,
                                duration: 1500,
                                complete: () => {
                                    if (params.callback) {
                                        setTimeout(() => {
                                            params.callback(queryResult.data)
                                        }, 1500)
                                    }
                                }
                            })
                        } else {
                            uni.showToast({
                                title: '支付失败！',
                                icon: 'warn',
                                mask: true,
                                duration: 1500,
                                complete: () => {
                                    if (params.callback) {
                                        setTimeout(() => {
                                            params.callback(null)
                                        }, 1500)
                                    }
                                }
                            })
                        }
                    })
                },
                fail: function (res) {
                    uni.showToast({
                        title: '支付失败！',
                        icon: 'warn',
                        mask: true,
                        duration: 1500,
                        complete: () => {
                            if (params.callback) {
                                setTimeout(() => {
                                    params.callback(null)
                                }, 1500)
                            }
                        }
                    })
                },
                complete: function (res) {
                    console.log(res)
                }
            })
        } else {  // 支付密钥生成失败
            uni.showToast({
                title: '支付失败！',
                icon: 'warn',
                duration: 1500,
                mask: true,
            })
        }
    })
}


/**
 * 微信支付-微信小程序版
 * 购买邮件支付码
 */
export function wxMiniAppPay_tbEmailPayCodeOrder(params) {
    const _self = this;
    HttpUtil.myPost({ // 封装微信请求方法
        url: 'wxPay/miniAppPay_tbEmailPayCodeOrder',
        data: {
            unionid: globalVariable.userInfo.unionid,
            email: params.email,
            type: params.type,
        }
    }).then(result => {

        // 支付密钥生成成功
        if (result.code === 0) {

            // 支付密钥生成成功，调起支付功能
            uni.requestPayment({
                timeStamp: result.data.timeStamp,
                nonceStr: result.data.nonceStr,
                package: result.data.package,
                signType: result.data.signType,
                paySign: result.data.paySign,
                success: (res) => {
                    // 支付成功！ 请求后台校验是否支付成功
                    HttpUtil.myPost({
                        url: 'wxPay/miniAppPay_tbEmailPayCodeOrderQuery',
                        data: {
                            code: result.data.code, // 订单编号
                        }
                    }).then(queryResult => {
                        if (queryResult.code === 0) { // 校验成功/支付成功
                            uni.showToast({
                                title: '支付成功！',
                                icon: 'success',
                                mask: true,
                                duration: 1500,
                                complete: () => {
                                    if (params.callback) {
                                        setTimeout(() => {
                                            params.callback(queryResult.data)
                                        }, 1500)
                                    }
                                }
                            })
                        } else {
                            uni.showToast({
                                title: '支付失败！',
                                icon: 'warn',
                                mask: true,
                                duration: 1500,
                                complete: () => {
                                    if (params.callback) {
                                        setTimeout(() => {
                                            params.callback(null)
                                        }, 1500)
                                    }
                                }
                            })
                        }
                    })
                },
                fail: function (res) {
                    uni.showToast({
                        title: '支付失败！',
                        icon: 'warn',
                        mask: true,
                        duration: 1500,
                        complete: () => {
                            if (params.callback) {
                                setTimeout(() => {
                                    params.callback(null)
                                }, 1500)
                            }
                        }
                    })
                },
                complete: function (res) {
                    console.log(res)
                }
            })
        } else {  // 支付密钥生成失败
            uni.showToast({
                title: '支付失败！',
                icon: 'warn',
                duration: 1500,
                mask: true,
            })
        }
    })
}


// todo ------------------------------------------------微信APP支付------------------------------------------------
// todo ------------------------------------------------微信APP支付------------------------------------------------
// todo ------------------------------------------------微信APP支付------------------------------------------------
// todo ------------------------------------------------微信APP支付------------------------------------------------
// todo ------------------------------------------------微信APP支付------------------------------------------------
/**
 * 微信支付-微信小程序版
 * 订单支付
 * @param params
 */
export function appPay(params) {
    HttpUtil.myPost({ // 封装微信请求方法
        url: 'wxPay/appPay',
        data: {
            unionid: globalVariable.userInfo.unionid,
            orderId: params.orderId, // 根据商户自己的秘钥进行支付，后台没有查询到则使用自己默认的。
        }
    }).then(result => {
        // 支付密钥生成成功
        if (result.code === 0) {
            // 支付密钥生成成功，调起支付功能
            uni.requestPayment({
                provider: 'wxpay',
                orderInfo: {  //微信、支付宝订单数据
                    appid: result.data.appid,
                    noncestr: result.data.noncestr,
                    package: result.data.package, // 固定值，以微信支付文档为主
                    partnerid: result.data.partnerid,
                    prepayid: result.data.prepayid,
                    timestamp: result.data.timestamp,
                    sign: result.data.sign // 根据签名算法生成签名
                },
                success: (res) => {

                    // 支付成功！ 请求后台校验是否支付成功
                    HttpUtil.myPost({
                        url: 'wxPay/appPayOrderQuery',
                        data: {
                            orderId: params.orderId
                        }
                    }).then(queryResult => {
                        if (queryResult.code === 0) { // 校验成功/支付成功
                            uni.showToast({
                                title: '支付成功！',
                                icon: 'success',
                                duration: 1500,
                                mask: true,
                                complete: () => {
                                    if (params.callback) {
                                        setTimeout(() => {
                                            params.callback(queryResult.data)
                                        }, 1500)
                                    }
                                }
                            })
                        } else {
                            uni.showToast({
                                title: '支付失败！',
                                icon: 'warn',
                                duration: 1500,
                                mask: true,
                                complete: () => {
                                    if (params.callback) {
                                        setTimeout(() => {
                                            params.callback(null)
                                        }, 1500)
                                    }
                                }
                            })
                        }

                    })
                },
                fail: function (res) {
                    uni.showToast({
                        title: '支付失败！',
                        icon: 'warn',
                        mask: true,
                        duration: 1500,
                        complete: () => {
                            if (params.callback) {
                                setTimeout(() => {
                                    params.callback(null)
                                }, 1500)
                            }
                        }
                    })
                },
                complete: function (res) {
                    console.log(res)
                }
            })

        } else {  // 支付密钥生成失败
            uni.showToast({
                title: '支付失败！',
                icon: 'warn',
                duration: 1500,
                mask: true,
            })
        }
    })
}

/**
 * 微信支付-微信小程序版
 * 充值店铺保证金
 */
export function appPay_tbStoreCashDeposit(params) {
    const _self = this;
    HttpUtil.myPost({ // 封装微信请求方法
        url: 'wxPay/appPay_tbStoreCashDeposit',
        data: {
            unionid: globalVariable.userInfo.unionid,
            payMoney: params.payMoney
        }
    }).then(result => {

        // 支付密钥生成成功
        if (result.code === 0) {
            // 支付密钥生成成功，调起支付功能
            uni.requestPayment({
                provider: 'wxpay',
                orderInfo: {  //微信、支付宝订单数据
                    appid: result.data.appid,
                    noncestr: result.data.noncestr,
                    package: result.data.package, // 固定值，以微信支付文档为主
                    partnerid: result.data.partnerid,
                    prepayid: result.data.prepayid,
                    timestamp: result.data.timestamp,
                    sign: result.data.sign // 根据签名算法生成签名
                },
                success: (res) => {
                    // 支付成功！ 请求后台校验是否支付成功
                    HttpUtil.myPost({
                        url: 'wxPay/appPay_tbStoreCashDepositQuery',
                        data: {
                            code: result.data.code, // 订单编号
                        }
                    }).then(queryResult => {
                        if (queryResult.code === 0) { // 校验成功/支付成功
                            uni.showToast({
                                title: '支付成功！',
                                icon: 'success',
                                mask: true,
                                duration: 1500,
                                complete: () => {
                                    if (params.callback) {
                                        setTimeout(() => {
                                            params.callback(queryResult.data)
                                        }, 1500)
                                    }
                                }
                            })
                        } else {
                            uni.showToast({
                                title: '支付失败！',
                                icon: 'warn',
                                mask: true,
                                duration: 1500,
                                complete: () => {
                                    if (params.callback) {
                                        setTimeout(() => {
                                            params.callback(null)
                                        }, 1500)
                                    }
                                }
                            })
                        }
                    })
                },
                fail: function (res) {
                    uni.showToast({
                        title: '支付失败！',
                        icon: 'warn',
                        mask: true,
                        duration: 1500,
                        complete: () => {
                            if (params.callback) {
                                setTimeout(() => {
                                    params.callback(null)
                                }, 1500)
                            }
                        }
                    })
                },
                complete: function (res) {
                    console.log(res)
                }
            })
        } else {  // 支付密钥生成失败
            uni.showToast({
                title: '支付失败！',
                icon: 'warn',
                duration: 1500,
                mask: true,
            })
        }
    })
}


/**
 * 微信支付-微信小程序版
 * 购买邮件支付码
 */
export function appPay_tbEmailPayCodeOrder(params) {
    const _self = this;
    HttpUtil.myPost({ // 封装微信请求方法
        url: 'wxPay/appPay_tbEmailPayCodeOrder',
        data: {
            unionid: globalVariable.userInfo.unionid,
            email: params.email,
            type: params.type,
        }
    }).then(result => {

        // 支付密钥生成成功
        if (result.code === 0) {
            // 支付密钥生成成功，调起支付功能
            uni.requestPayment({
                provider: 'wxpay',
                orderInfo: {  //微信、支付宝订单数据
                    appid: result.data.appid,
                    noncestr: result.data.noncestr,
                    package: result.data.package, // 固定值，以微信支付文档为主
                    partnerid: result.data.partnerid,
                    prepayid: result.data.prepayid,
                    timestamp: result.data.timestamp,
                    sign: result.data.sign // 根据签名算法生成签名
                },
                success: (res) => {
                    // 支付成功！ 请求后台校验是否支付成功
                    HttpUtil.myPost({
                        url: 'wxPay/appPay_tbEmailPayCodeOrderQuery',
                        data: {
                            code: result.data.code, // 订单编号
                        }
                    }).then(queryResult => {
                        if (queryResult.code === 0) { // 校验成功/支付成功
                            uni.showToast({
                                title: '支付成功！',
                                icon: 'success',
                                mask: true,
                                duration: 1500,
                                complete: () => {
                                    if (params.callback) {
                                        setTimeout(() => {
                                            params.callback(queryResult.data)
                                        }, 1500)
                                    }
                                }
                            })
                        } else {
                            uni.showToast({
                                title: '支付失败！',
                                icon: 'warn',
                                mask: true,
                                duration: 1500,
                                complete: () => {
                                    if (params.callback) {
                                        setTimeout(() => {
                                            params.callback(null)
                                        }, 1500)
                                    }
                                }
                            })
                        }
                    })
                },
                fail: function (res) {
                    uni.showToast({
                        title: '支付失败！',
                        icon: 'warn',
                        mask: true,
                        duration: 1500,
                        complete: () => {
                            if (params.callback) {
                                setTimeout(() => {
                                    params.callback(null)
                                }, 1500)
                            }
                        }
                    })
                },
                complete: function (res) {
                    console.log(res)
                }
            })
        } else {  // 支付密钥生成失败
            uni.showToast({
                title: '支付失败！',
                icon: 'warn',
                duration: 1500,
                mask: true,
            })
        }
    })
}




