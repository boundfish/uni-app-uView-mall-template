import {CommonService} from "../../services/common.service";
import {CommonUtil} from "./common.util";
import {HttpUtil} from "./http.util";

const qiniuUploader = require("./qiniuUploader");

/**
 * 七牛云工具类
 */
export class QiniuCloudUtil {

    static base64Encode(str) {
        // 对字符串进行编码
        var encode = encodeURI(str);
        // 对编码的字符串转化base64
        // var base64 = btoa(encode); // 微信小程序不支持
        var base64 = CommonUtil.toBase64Encode(encode);
        return base64;
    }

    /**
     * 帮助文档：https://github.com/gpake/qiniu-wxapp-sdk
     * @param params
     * @returns {Promise<any>}
     */
    static upload(params) {
        const _self = this;
        if (!params.id) {
            params.id = CommonUtil.generateUUID();
        }
        return new Promise(function (resolve, reject) {
            new CommonService().findQiNiuUploadToken().then(result => {
                // 交给七牛上传
                qiniuUploader.upload(params.filePath,
                    (res) => { // todo 上传成功
                        // 每个文件上传成功后,处理相关的事情
                        // 其中 info 是文件上传成功后，服务端返回的json，形式如
                        // {
                        //    "hash": "Fh8xVqod2MQ1mocfI4S4KpRL6D98",
                        //    "key": "gogopher.jpg"
                        //  }
                        // 参考http://developer.qiniu.com/docs/v6/api/overview/up/response/simple-response.html
                        console.log('file url is: ' + res.fileUrl);
                        resolve({
                            id: params.id,
                            status: "success"
                        });
                    }, (error) => {  // todo 上传失败
                        console.log('error: ' + error);
                        resolve({
                            id: params.id,
                            status: "fail"
                        });
                    },
                    {
                        region: 'SCN',  // qiniuUploader.js文件 uploadURLFromRegionCode 方法可以看到
                        domain: 'http://image.xxxxxxx.com', // // bucket 域名，下载资源时用到。如果设置，会在 success callback 的 res 参数加上可以直接使用的 ImageURL 字段。否则需要自己拼接
                        key: params.id, // todo 对应sysFile主键 [非必须]自定义文件 key。如果不设置，默认为使用微信小程序 API 的临时文件名
                        // 以下方法三选一即可，优先级为：uptoken > uptokenURL > uptokenFunc
                        uptoken: result.token, // 由其他程序生成七牛 uptoken
                    }, (res) => {
                        console.log('上传进度', res.progress)
                        console.log('已经上传的数据长度', res.totalBytesSent)
                        console.log('预期需要上传的数据总长度', res.totalBytesExpectedToSend)
                    }, () => {
                        // 取消上传
                    }, () => {
                        // `before` 上传前执行的操作
                    }, (err) => {
                        // `complete` 上传接受后执行的操作(无论成功还是失败都执行)
                    });
            });
        });
    }

    /**
     * 将图片上传到七牛云，并将上传的图片id保存到后台服务端。
     * @param fkId sysFile表的外键fkId
     * @param filePath 图片路径
     * @param data
     * @returns {Promise<any>}
     */
    static uploadToCloud(fkId, filePath, data = {clearOldImage: true, typeMark: "thumbnail"}) {
        const _self = this;
        return new Promise(function (resolve, reject) {
            _self.upload({
                filePath: filePath
            }).then(qiniuCloudResult => {
                console.log(qiniuCloudResult);
                if (qiniuCloudResult.status === "success") {
                    HttpUtil.myPost({
                        url: `sysfile/saveBase64Image`,
                        data: {
                            id: qiniuCloudResult.id,
                            name: "移动端上传的图片",
                            fkId: fkId,
                            type: 'image/jpeg',
                            clearOldImage: data.clearOldImage,
                            typeMark: data.typeMark,
                            content: "jsUploaded"  // 在js端就将图片上传到七牛云了
                        }
                    }).then(result => {
                        /*let 后台返回 = {
                            id: "sysFile.id",
                            sysFile: sysFile,
                            status: "success"
                        };*/
                        result.message = "上传图片成功";
                        resolve(result);
                    });
                } else {
                    resolve({
                        status: "fail",
                        message: "上传图片失败"
                    });
                }
            })
        });
    }
}
