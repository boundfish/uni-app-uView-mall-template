import Vue from 'vue'
import globalVariable from "../global-variable";

/**
 * todo 网络超时设置方式：https://uniapp.dcloud.net.cn/collocation/manifest?id=networktimeout
 * 在配置文件manifest.json中配置
 * "networkTimeout":{
 *      "request": 600000  单位/毫秒
 *  },
 *  "app-plus" : {
 *  }
 */
export class HttpUtil {

    /**
     * 版本: ‘develop’, //开发版
     * 版本: ‘trial’, //体验版
     * 版本: ‘release’, //正式版
     */
    static appendProfilesActive(params) {
        let profilesActive = '';
        if (this.__wxConfig) {
            if (__wxConfig.envVersion === 'develop') {
                profilesActive = 'dev';
            } else if (__wxConfig.envVersion === 'trial') {
                profilesActive = 'dev';  // test
            } else if (__wxConfig.envVersion === 'release') {
                profilesActive = 'prod';
            }
            if (params.data) {
                params.data.profilesActive = profilesActive;
            }
        }
    }

    static getRequestParams(params) {
        if (params) {
            if (typeof params === 'string') {
                params = {url: params}
            }

            if (params.url.indexOf('http') === -1) {
                params.url = globalVariable.baseUrl + params.url
            }

            // this.appendProfilesActive(params);

            let data = params.data;
            if (!data) {
                params.data = {};
                return params
            }

            // 删除data参数中值为null或者空字符串或者是undefined的属性。
            if (params.isOptimizeData !== false) {  // 是否优化参数
                // 对于更新的请求，不显示优化属性的话，一般不删除空字符串的属性。
                if (params.url.substring(params.url.lastIndexOf('/') + 1).indexOf('update') === 0 && params.isOptimizeData !== true) {
                    return params
                }
                for (let attrName in data) {
                    if (data[attrName] === undefined
                        || data[attrName] == null
                        || data[attrName] === 'undefined'
                        || data[attrName] === 'null'
                        || (data[attrName] + '').trim() === '') {
                        delete data[attrName]
                    }
                }
            }
        }
        return params
    }

    /**
     * post请求时将url附加的参数追加到data中
     * 2020-8-31 17:12:45 uView框架post请求url附加参数会报错，运行时就卡死。
     * @param params
     */
    static urlParamsToData(params) {
        if (params.url.indexOf("?") !== -1) {  // url=test?a=1&b=2
            let url = params.url.substring(0, params.url.indexOf("?"));  // url=test
            let keyValueStr = params.url.substring(params.url.indexOf("?") + 1);  // keyValueStr -> a=1&b=2
            let keyValueList = keyValueStr.split("&");  // keyValueList=["a=1", "b=2"]
            // 去掉url中的参数
            params.url = url;
            // 将url中的参数追加到data中
            keyValueList.forEach((value, index) => {
                // todo 覆盖data中同属性值
                params.data[value.split("=")[0]] = value.split("=")[1]
            })
        }
    }

    static myGet(params) {
        const _self = this;
        params = _self.getRequestParams(params);
        return Vue.prototype.$u.get(params.url, params.data)
    }

    static myPost(params) {
        const _self = this;
        params = _self.getRequestParams(params);
        _self.urlParamsToData(params);
        return Vue.prototype.$u.post(params.url, params.data, {
            'content-type': 'application/x-www-form-urlencoded; charset=UTF-8'
        })
    }

    static myPostJson(params) {
        const _self = this;
        params = _self.getRequestParams(params);
        _self.urlParamsToData(params);
        return Vue.prototype.$u.post(params.url, params.data, {
            'content-type': 'application/json; charset=UTF-8'
        })
    }
}

