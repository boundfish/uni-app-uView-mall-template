import globalVariable from "../global-variable";

export class TabbarUtil {

    static getTabbar() {
        let list = [
            {
                type: "0",
                tabbar: [
                    {
                        "componentRef": "indexRef",
                        "text": "首页",
                        "title": globalVariable.userInfo.tbCommunity ? globalVariable.userInfo.tbCommunity.name : '首页',
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/home_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/home_fill_light.png"
                    },
                    {
                        "componentRef": "goodsClassRef",
                        "text": "分类",
                        "title": "商品分类",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/sort_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/sort.png"
                    },
                    {
                        "componentRef": "shoppingCartRef",
                        "text": "购物车",
                        "title": "购物车",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/cart_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/cart_fill_light.png"
                    },
                    {
                        "componentRef": "communityDynamicRef",
                        "text": "动态",
                        "title": "商城动态",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/community_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/community_fill_light.png"
                    },
                    {
                        "componentRef": "personRef",
                        "text": "我的",
                        "title": "我的",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/my_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/my_fill_light.png"
                    }
                ]
            },
            {
                type: "1",
                tabbar: [
                    {
                        "componentRef": "indexRef",
                        "text": "首页",
                        "title": globalVariable.userInfo.tbCommunity ? globalVariable.userInfo.tbCommunity.name : '首页',
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/home_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/home_fill_light.png"
                    },
                    {
                        "componentRef": "goodsClassRef",
                        "text": "分类",
                        "title": "商品分类",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/sort_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/sort.png"
                    },
                    {
                        "componentRef": "shoppingCartRef",
                        "text": "购物车",
                        "title": "购物车",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/cart_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/cart_fill_light.png"
                    },
                    {
                        "componentRef": "personRef",
                        "text": "我的",
                        "title": "我的",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/my_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/my_fill_light.png"
                    }
                ]
            },
            {
                type: "2",
                tabbar: [
                    {
                        "componentRef": "indexRef",
                        "text": "首页",
                        "title": globalVariable.userInfo.tbCommunity ? globalVariable.userInfo.tbCommunity.name : '首页',
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/home_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/home_fill_light.png"
                    },
                    {
                        "componentRef": "goodsClassRef",
                        "text": "分类",
                        "title": "商品分类",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/sort_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/sort.png"
                    },
                    {
                        "componentRef": "goodsSeckillRef",
                        "text": "商品秒杀",
                        "title": "商品秒杀",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/seckill_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/seckill_fill.png"
                    },
                    {
                        "componentRef": "shoppingCartRef",
                        "text": "购物车",
                        "title": "购物车",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/cart_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/cart_fill_light.png"
                    },
                    {
                        "componentRef": "personRef",
                        "text": "我的",
                        "title": "我的",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/my_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/my_fill_light.png"
                    }
                ]
            },
            {
                type: "3",
                tabbar: [
                    {
                        "componentRef": "indexEateryRef",
                        "text": "首页",
                        "title": globalVariable.userInfo.tbCommunity ? globalVariable.userInfo.tbCommunity.name : '首页',
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/home_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/home_fill_light.png"
                    },
                    {
                        "componentRef": "shoppingCartRef",
                        "text": "购物车",
                        "title": "购物车",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/cart_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/cart_fill_light.png"
                    },
                    {
                        "componentRef": "personRef",
                        "text": "我的",
                        "title": "我的",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/my_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/my_fill_light.png"
                    }
                ]
            },
            {
                type: "4",
                tabbar: [
                    {
                        "componentRef": "indexEateryRef",
                        "text": "首页",
                        "title": globalVariable.userInfo.tbCommunity ? globalVariable.userInfo.tbCommunity.name : '首页',
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/home_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/home_fill_light.png"
                    },
                    {
                        "componentRef": "shoppingCartRef",
                        "text": "购物车",
                        "title": "购物车",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/cart_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/cart_fill_light.png"
                    },
                    {
                        "componentRef": "communityDynamicRef",
                        "text": "动态",
                        "title": "商城动态",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/community_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/community_fill_light.png"
                    },
                    {
                        "componentRef": "personRef",
                        "text": "我的",
                        "title": "我的",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/my_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/my_fill_light.png"
                    }
                ]
            },
            {
                type: "5",
                tabbar: [
                    {
                        "componentRef": "indexEateryRef",
                        "text": "首页",
                        "title": globalVariable.userInfo.tbCommunity ? globalVariable.userInfo.tbCommunity.name : '首页',
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/home_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/home_fill_light.png"
                    },
                    {
                        "componentRef": "goodsSeckillRef",
                        "text": "商品秒杀",
                        "title": "商品秒杀",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/seckill_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/seckill_fill.png"
                    },
                    {
                        "componentRef": "shoppingCartRef",
                        "text": "购物车",
                        "title": "购物车",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/cart_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/cart_fill_light.png"
                    },
                    {
                        "componentRef": "personRef",
                        "text": "我的",
                        "title": "我的",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/my_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/my_fill_light.png"
                    }
                ]
            },
            {
                type: "6",
                tabbar: [
                    {
                        "componentRef": "indexEateryRef",
                        "text": "首页",
                        "title": globalVariable.userInfo.tbCommunity ? globalVariable.userInfo.tbCommunity.name : '首页',
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/home_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/home_fill_light.png"
                    },
                    {
                        "componentRef": "shoppingCartRef",
                        "text": "购物车",
                        "title": "购物车",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/cart_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/cart_fill_light.png"
                    },
                    {
                        "componentRef": "goodsSeckillRef",
                        "text": "商品秒杀",
                        "title": "商品秒杀",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/seckill_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/seckill_fill.png"
                    },
                    {
                        "componentRef": "communityDynamicRef",
                        "text": "动态",
                        "title": "商城动态",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/community_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/community_fill_light.png"
                    },
                    {
                        "componentRef": "personRef",
                        "text": "我的",
                        "title": "我的",
                        "iconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/my_light.png",
                        "selectedIconPath": "/pages/sub/sub1-tabs/static/icon/tabbar/tabs/my_fill_light.png"
                    }
                ]
            }
        ];

        // 更新选中的底部导航栏
        let tabbar = list[0].tabbar;
        list.forEach((item, index) => {
            if (item.type === globalVariable.userInfo.tbCommunity.tabbarType) {
                tabbar = item.tabbar;
            }
        });

        return tabbar;

        /*// 更新选择的底部导航栏tab
        let routes = getCurrentPages(); // 获取当前打开过的页面路由数组
        let curRoute = routes[routes.length - 1].route; //获取当前页面路由
        _self.tabbar.forEach((item, index) => {
            console.log(item.pagePath, curRoute);
            if (item.pagePath === curRoute) {
                _self.current = index;
            }
        });

        console.log('TabbarUtil---------------------------------', TabbarUtil.current, TabbarUtil.tabbar);*/
    }

    static getIndexByComponentRef(componentRef) {
        const _self = this;
        let tarbar = _self.getTabbar();
        tarbar.forEach((item, index) => {
            if (item.componentRef === componentRef) {
                return index;
            }
        })
    }
}
