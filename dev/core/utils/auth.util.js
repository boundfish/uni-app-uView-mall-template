import globalVariable from "../global-variable";
import {LocalStorageUtil} from "./local-storage.util";
import {CommonUtil} from "./common.util";

export class AuthUtil {

    /**
     * 退出登录
     */
    static logout(message = "会话已过期，请重新登录") {
        globalVariable.token = null;
        globalVariable.userInfo = {};
        globalVariable.orderSequenceNumber = null;
        globalVariable.coordinate = null;
        LocalStorageUtil.removeItem("token");
        LocalStorageUtil.removeItem("userInfo");
        CommonUtil.toastReLaunch(message, "/pages/main/login/index");
    }
}
